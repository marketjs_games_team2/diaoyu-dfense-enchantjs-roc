var Defender = enchant.Class.create(enchant.Sprite, {

	initialize: function(x, y) {
		
		enchant.Sprite.call(this);
		this.width=72;
		this.height=50;
		this.x = x;
		this.y = y;	
		this.image = game.assets["media/island_canon.png"];
		this.scaleX=-1;
		this.frame = 0;
		
		//game.rootScene.addChild(this);
	},

});