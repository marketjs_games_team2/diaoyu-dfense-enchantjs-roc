  1 /**
  2  * @fileOverview
  3  * gl.enchant.js
  4  * @version 0.3.6
  5  * @require enchant.js v0.4.5+
  6  * @require gl-matrix.js 1.3.7+
  7  * @author Ubiquitous Entertainment Inc.
  8  *
  9  * @description
 10  * Drawing library using WebGL
 11  * By combining with enchant.js, high quality 3D drawing and combination with 2D drawing is possible
 12  *
 13  * @detail
 14  * Uses gl-matrix.js in vector, matrix operation.
 15  * gl-matrix.js:
 16  * https://github.com/toji/gl-matrix/
 17  *
 18  */
 19 
 20 /**
 21  * Exports gl.enchant.js class to enchant.
 22  */
 23 enchant.gl = {};
 24 
 25 if (typeof glMatrixArrayType === 'undefined') {
 26     throw new Error('should load gl-matrix.js before loading gl.enchant.js');
 27 }
 28 
 29 (function() {
 30 
 31     var CONTEXT_NAME = 'experimental-webgl';
 32 
 33     var parentModule = null;
 34     (function() {
 35         enchant();
 36         if (enchant.nineleap !== undefined) {
 37             if (enchant.nineleap.memory !== undefined &&
 38                 Object.getPrototypeOf(enchant.nineleap.memory) === Object.prototype) {
 39                 parentModule = enchant.nineleap.memory;
 40             } else if (enchant.nineleap !== undefined &&
 41                 Object.getPrototypeOf(enchant.nineleap) === Object.prototype) {
 42                 parentModule = enchant.nineleap;
 43             }
 44         } else {
 45             parentModule = enchant;
 46         }
 47     }());
 48 
 49     enchant.gl.Core = enchant.Class.create(parentModule.Core, {
 50         initialize: function(width, height) {
 51             parentModule.Core.call(this, width, height);
 52             this.GL = new GLUtil();
 53             this.currentScene3D = null;
 54             this.addEventListener('enterframe', function(e) {
 55                 if (!this.currentScene3D) {
 56                     return;
 57                 }
 58                 var nodes = this.currentScene3D.childNodes.slice();
 59                 var push = Array.prototype.push;
 60                 while (nodes.length) {
 61                     var node = nodes.pop();
 62                     node.dispatchEvent(e);
 63                     node.age++;
 64                     if (node.childNodes) {
 65                         push.apply(nodes, node.childNodes);
 66                     }
 67                 }
 68             });
 69         },
 70         debug: function() {
 71             this.GL._enableDebugContext();
 72             this._debug = true;
 73             this.addEventListener("enterframe", function(time) {
 74                 this._actualFps = (1000 / time.elapsed);
 75             });
 76             this.start();
 77         }
 78     });
 79 
 80     var GLUtil = enchant.Class.create({
 81         initialize: function() {
 82             var core = enchant.Core.instance;
 83             if (typeof core.GL !== 'undefined') {
 84                 return core.GL;
 85             }
 86             this._createStage(core.width, core.height, core.scale);
 87             this._prepare();
 88             this.textureManager = new TextureManager();
 89             this.detectColorManager = new DetectColorManager();
 90             this.detectFrameBuffer = new enchant.gl.FrameBuffer(core.width, core.height);
 91             this.defaultProgram = new enchant.gl.Shader(DEFAULT_VERTEX_SHADER_SOURCE, DEFAULT_FRAGMENT_SHADER_SOURCE);
 92             this.setDefaultProgram();
 93         },
 94         setDefaultProgram: function() {
 95             this.setProgram(this.defaultProgram);
 96         },
 97         setProgram: function(program) {
 98             program.use();
 99             this.currentProgram = program;
100         },
101         _prepare: function() {
102             var width = this._canvas.width;
103             var height = this._canvas.height;
104             gl.clearColor(0.0, 0.0, 0.0, 1.0);
105             gl.viewport(0, 0, width, height);
106             gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
107             gl.enable(gl.DEPTH_TEST);
108             gl.enable(gl.CULL_FACE);
109         },
110         _createStage: function(width, height, scale) {
111             var div = createParentDiv();
112             var that = this;
113             var stage = document.getElementById('enchant-stage');
114             var cvs = this._canvas = createGLCanvas(width, height, scale);
115             var detect = new enchant.Sprite(width, height);
116             var core = enchant.Core.instance;
117             (function() {
118                 var color = new Uint8Array(4);
119                 var touching = null;
120                 var sprite;
121                 detect.addEventListener('touchstart', function(e) {
122                     var scene = core.currentScene3D;
123                     var x = parseInt(e.x, 10);
124                     var y = parseInt(this.height - e.y, 10);
125                     that.detectFrameBuffer.bind();
126                     scene._draw('detect');
127                     gl.readPixels(x, y, 1, 1, gl.RGBA, gl.UNSIGNED_BYTE, color);
128                     sprite = that.detectColorManager.getSpriteByColor(color);
129                     if (sprite) {
130                         touching = sprite;
131                         touching.dispatchEvent(e);
132                     }
133                     that.detectFrameBuffer.unbind();
134                 });
135                 detect.addEventListener('touchmove', function(e) {
136                     if (touching !== null) {
137                         touching.dispatchEvent(e);
138                     }
139                 });
140                 detect.addEventListener('touchend', function(e) {
141                     if (touching !== null) {
142                         touching.dispatchEvent(e);
143                     }
144                     touching = null;
145                 });
146             }());
147             window['gl'] = this._gl = this._getContext(cvs);
148             div.appendChild(cvs);
149             stage.insertBefore(div, core.rootScene._element);
150             core.rootScene.addChild(detect);
151         },
152         _getContext: function(canvas, debug) {
153             var ctx = canvas.getContext(CONTEXT_NAME);
154             if (!ctx) {
155                 window['alert']('could not initialized WebGL');
156                 throw new Error('could not initialized WebGL');
157             }
158             if (debug) {
159                 ctx = createDebugContext(ctx);
160             }
161             return ctx;
162         },
163         _enableDebugContext: function() {
164             window['gl'] = this._gl = createDebugContext(this._gl);
165         },
166         parseColor: function(string) {
167             return parseColor(string);
168         },
169         renderElements: function(buffer, offset, length, attributes, uniforms) {
170             if (attributes) {
171                 this.currentProgram.setAttributes(attributes);
172             }
173             if (uniforms) {
174                 this.currentProgram.setUniforms(uniforms);
175             }
176             buffer.bind();
177             gl.drawElements(gl.TRIANGLES, length, gl.UNSIGNED_SHORT, offset);
178             buffer.unbind();
179         }
180     });
181 
182     var parseColor = function(string) {
183         var color = [];
184         if (typeof string === 'string') {
185             if (string.match(/#/)) {
186                 string.match(/[0-9a-fA-F]{2}/g).forEach(function(n) {
187                     color[color.length] = ('0x' + n - 0) / 255;
188                 });
189                 color[color.length] = 1.0;
190             } else if (string.match(/rgba/)) {
191                 string.match(/[0-9]{1,3},/g).forEach(function(n) {
192                     color[color.length] = parseInt(n, 10) / 255;
193                 });
194                 color[color.length] = parseFloat(string.match(/[0-9]\.[0-9]{1,}/)[0]);
195             } else if (string.match(/rgb/)) {
196                 string.match(/[0-9]{1,3},/g).forEach(function(n) {
197                     color[color.length] = parseInt(n, 10) / 255;
198                 });
199                 color[color.length] = 1.0;
200             }
201         } else if (string instanceof Array) {
202             color = string;
203         }
204         return color;
205     };
206 
207     var createDebugContext = function(context) {
208         var ctx = {};
209         var names = {};
210         var type = '';
211         var val;
212         var makeFakedMethod = function(context, prop) {
213             return function() {
214                 var value, error;
215                 value = context[prop].apply(context, arguments);
216                 error = context.getError();
217                 if (error) {
218                     window['console'].log(names[error] + '(' + error + ')' + ': ' + prop);
219                     window['console'].log(arguments);
220                 }
221                 return value;
222             };
223         };
224         for (var prop in context) {
225             type = typeof context[prop];
226             val = context[prop];
227             if (type === 'function') {
228                 ctx[prop] = makeFakedMethod(context, prop);
229             } else if (type === 'number') {
230                 names[val] = prop;
231                 ctx[prop] = val;
232             } else {
233                 ctx[prop] = val;
234             }
235         }
236         ctx.getNameById = function(i) {
237             return names[i];
238         };
239         return ctx;
240     };
241 
242     var createParentDiv = function() {
243         var div = document.createElement('div');
244         div.style['position'] = 'absolute';
245         div.style['z-index'] = -1;
246         div.style[enchant.ENV.VENDOR_PREFIX + 'TransformOrigin'] = '0 0';
247         return div;
248     };
249 
250     var createGLCanvas = function(width, height, scale) {
251         var cvs = document.createElement('canvas');
252         cvs.width = width;
253         cvs.height = height;
254         cvs.style['position'] = 'absolute';
255         cvs.style['z-index'] = -1;
256         cvs.style[enchant.ENV.VENDOR_PREFIX + 'Transform'] = 'scale(' + scale + ')';
257         cvs.style[enchant.ENV.VENDOR_PREFIX + 'TransformOrigin'] = '0 0';
258         return cvs;
259     };
260 
261     var TextureManager = enchant.Class.create({
262         initialize: function() {
263             this.storage = {};
264         },
265         hasTexture: function(src) {
266             return src in this.storage;
267         },
268         getWebGLTexture: function(image, flip, wrap, mipmap) {
269             var ret;
270             if (this.hasTexture(image.src)) {
271                 ret = this.storage[image.src];
272             } else {
273                 ret = this.createWebGLTexture(image, flip, wrap, mipmap);
274             }
275             return ret;
276         },
277         isPowerOfTwo: function(n) {
278             return (n > 0) && ((n & (n - 1)) === 0);
279         },
280         setTextureParameter: function(power, target, wrap, mipmap) {
281             var filter;
282             if (mipmap) {
283                 filter = gl.LINEAR_MIPMAP_LINEAR;
284             } else {
285                 filter = gl.NEAREST;
286             }
287             if (!power) {
288                 wrap = gl.CLAMP_TO_EDGE;
289             }
290             gl.texParameteri(target, gl.TEXTURE_WRAP_S, wrap);
291             gl.texParameteri(target, gl.TEXTURE_WRAP_T, wrap);
292             gl.texParameteri(target, gl.TEXTURE_MAG_FILTER, filter);
293             gl.texParameteri(target, gl.TEXTURE_MIN_FILTER, filter);
294         },
295         _texImage: function(image, target) {
296             gl.texImage2D(target, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
297         },
298         _writeWebGLTexture: function(image, target, wrap, mipmap) {
299             var power = this.isPowerOfTwo(image.width) && this.isPowerOfTwo(image.height);
300             if (typeof target === 'undefined') {
301                 target = gl.TEXTURE_2D;
302             }
303             if (typeof wrap === 'undefined') {
304                 wrap = gl.REPEAT;
305             }
306             this.setTextureParameter(power, target, wrap, mipmap);
307 
308             this._texImage(image, target);
309             if (mipmap) {
310                 gl.generateMipmap(target);
311             }
312         },
313         createWebGLTexture: function(image, flip, wrap, mipmap) {
314             var tex = gl.createTexture();
315             var target = gl.TEXTURE_2D;
316             gl.bindTexture(target, tex);
317             gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, flip);
318             this._writeWebGLTexture(image, target, wrap, mipmap);
319             gl.bindTexture(target, null);
320             this.storage[image.src] = tex;
321             return tex;
322         },
323         createWebGLCubeMapTexture: function(images, wrap, mipmap) {
324             var faceTargets = [
325                 gl.TEXTURE_CUBE_MAP_POSITIVE_X,
326                 gl.TEXTURE_CUBE_MAP_NEGATIVE_X,
327                 gl.TEXTURE_CUBE_MAP_POSITIVE_Y,
328                 gl.TEXTURE_CUBE_MAP_POSITIVE_Y,
329                 gl.TEXTURE_CUBE_MAP_NEGATIVE_Z,
330                 gl.TEXTURE_CUBE_MAP_NEGATIVE_Z
331             ];
332 
333             var tex = gl.createTexture();
334             var target, image;
335             gl.bindTexture(gl.TEXTURE_CUBE_MAP, tex);
336             gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
337             for (var i = 0, l = images.length; i < l; i++) {
338                 target = faceTargets[i];
339                 image = images[i];
340                 this._writeWebGLTexture(image, target, wrap, mipmap);
341             }
342             gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
343             return tex;
344         }
345     });
346 
347     var DetectColorManager = enchant.Class.create({
348         initialize: function() {
349             this.reference = [];
350             this.detectColorNum = 0;
351         },
352         attachDetectColor: function(sprite) {
353             this.detectColorNum += 1;
354             this.reference[this.detectColorNum] = sprite;
355             return this._createNewColor();
356         },
357         _createNewColor: function() {
358             var n = this.detectColorNum;
359             return [
360                 parseInt(n / 65536, 10) / 255,
361                 parseInt(n / 256, 10) / 255,
362                 parseInt(n % 256, 10) / 255, 1.0
363             ];
364         },
365         _decodeDetectColor: function(color) {
366             return Math.floor(color[0] * 65536) +
367                 Math.floor(color[1] * 256) +
368                 Math.floor(color[2]);
369         },
370         getSpriteByColor: function(color) {
371             return this.reference[this._decodeDetectColor(color)];
372         }
373     });
374 
375     /**
376      * @scope enchant.gl.Framebuffer.prototype
377      */
378     enchant.gl.FrameBuffer = enchant.Class.create({
379         /**
380          * Class for controlling WebGL frame buffers
381          * @param {String} width Frame buffer width
382          * @param {String} height Frame buffer height
383          * @constructs
384          */
385         initialize: function(width, height) {
386             var core = enchant.Core.instance;
387             if (typeof width === 'undefined') {
388                 width = core.width;
389             }
390             if (typeof height === 'undefined') {
391                 height = core.height;
392             }
393             this.framebuffer = gl.createFramebuffer();
394             this.colorbuffer = gl.createRenderbuffer();
395             this.depthbuffer = gl.createRenderbuffer();
396 
397             gl.bindFramebuffer(gl.FRAMEBUFFER, this.framebuffer);
398 
399             gl.bindRenderbuffer(gl.RENDERBUFFER, this.colorbuffer);
400             gl.renderbufferStorage(gl.RENDERBUFFER, gl.RGBA4, width, height);
401             gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.RENDERBUFFER, this.colorbuffer);
402 
403             gl.bindRenderbuffer(gl.RENDERBUFFER, this.depthbuffer);
404             gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, width, height);
405             gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, this.depthbuffer);
406 
407             gl.bindFramebuffer(gl.FRAMEBUFFER, null);
408             gl.bindRenderbuffer(gl.RENDERBUFFER, null);
409         },
410         /**
411          * Bind frame buffer.
412          */
413         bind: function() {
414             gl.bindFramebuffer(gl.FRAMEBUFFER, this.framebuffer);
415         },
416         /**
417          * Unbind frame buffer.
418          */
419         unbind: function() {
420             gl.bindFramebuffer(gl.FRAMEBUFFER, null);
421         },
422         /**
423          * Destroy object.
424          */
425         destroy: function() {
426             gl.deleteFramebuffer(this.framebuffer);
427             gl.deleteFramebuffer(this.colorbuffer);
428             gl.deleteFramebuffer(this.depthbuffer);
429         }
430     });
431 
432     /**
433      * @scope enchant.gl.Shader.prototype
434      */
435     enchant.gl.Shader = enchant.Class.create({
436         /**
437          * Class to control WebGL shader program.
438          * A shader program will be created by delivering the vertex shader source and fragment shader source.
439          * @param {String} vshader Vertex shader source
440          * @param {String} fshader Fragment shader source
441          * @constructs
442          */
443         initialize: function(vshader, fshader) {
444             this._vShaderSource = '';
445             this._fShaderSource = '';
446             this._updatedVShaderSource = false;
447             this._updatedFShaderSource = false;
448             this._vShaderProgram = null;
449             this._fShaderProgram = null;
450             this._program = null;
451             this._uniforms = {};
452             this._attributes = {};
453             this._attribLocs = {};
454             this._samplersNum = 0;
455 
456             if (typeof vshader === 'string') {
457                 this.vShaderSource = vshader;
458             }
459             if (typeof fshader === 'string') {
460                 this.fShaderSource = fshader;
461             }
462             if (this._updatedVShaderSource && this._updatedFShaderSource) {
463                 this.compile();
464             }
465         },
466         /**
467          * Vertex shader source
468          * @type String
469          */
470         vShaderSource: {
471             get: function() {
472                 return this._vShaderSource;
473             },
474             set: function(string) {
475                 this._vShaderSource = string;
476                 this._updatedVShaderSource = true;
477             }
478         },
479         /**
480          * Fragment shader source
481          * @type String
482          */
483         fShaderSource: {
484             get: function() {
485                 return this._fShaderSource;
486             },
487             set: function(string) {
488                 this._fShaderSource = string;
489                 this._updatedFShaderSource = true;
490             }
491         },
492         /**
493          * Compile shader program.
494          * Will be automatically compiled when shader source is delivered from constructor.
495          * @example
496          * var shader = new Shader();
497          * // Deliver shader program source.
498          * shader.vShaderSource = vert;
499          * shader.fShaderSource = frag;
500          * // Compile.
501          * shader.compile();
502          */
503         compile: function() {
504             if (this._updatedVShaderSource) {
505                 this._prepareVShader();
506             }
507             if (this._updatedFShaderSource) {
508                 this._prepareFShader();
509             }
510             if (this._program === null) {
511                 this._program = gl.createProgram();
512             } else {
513                 gl.detachShader(this._program, this._vShaderProgram);
514                 gl.detachShader(this._program, this._fShaderProgram);
515             }
516             gl.attachShader(this._program, this._vShaderProgram);
517             gl.attachShader(this._program, this._fShaderProgram);
518             gl.linkProgram(this._program);
519             if (!gl.getProgramParameter(this._program, gl.LINK_STATUS)) {
520                 this._logShadersInfo();
521                 throw 'could not compile shader';
522             }
523             this._getAttributesProperties();
524             this._getUniformsProperties();
525         },
526         /**
527          * Set shader program to be used.
528          */
529         use: function() {
530             gl.useProgram(this._program);
531         },
532         /**
533          * Sets attribute variables to shader program.
534          * Used in enchant.gl.Sprite3D contents and elsewhere.
535          * @param {*} params Level
536          * @example
537          * var shader = new Shader(vert, frag);
538          * shader.setAttributes({
539          *     aVertexPosition: indices,
540          *     aNormal: normals
541          * });
542          */
543         setAttributes: function(params) {
544             for (var prop in params) {
545                 if (params.hasOwnProperty(prop)) {
546                     this._attributes[prop] = params[prop];
547                 }
548             }
549         },
550         /**
551          * Set uniform variables to shader program.
552          * Used in enchant.gl.Sprite3D and elsewhere.
553          * @param {*} params Level
554          * @example
555          * var shader = new Shader(vert, frag);
556          * shader.setUniforms({
557          *     uDiffuse: diffuse,
558          *     uLightColor: lightColor
559          * });
560          */
561         setUniforms: function(params) {
562             for (var prop in params) {
563                 if (params.hasOwnProperty(prop)) {
564                     this._uniforms[prop] = params[prop];
565                 }
566             }
567         },
568         _prepareVShader: function() {
569             if (this._vShaderProgram === null) {
570                 this._vShaderProgram = gl.createShader(gl.VERTEX_SHADER);
571             }
572             gl.shaderSource(this._vShaderProgram, this._vShaderSource);
573             gl.compileShader(this._vShaderProgram);
574             this._updatedVShaderSource = false;
575         },
576         _prepareFShader: function() {
577             if (this._fShaderProgram === null) {
578                 this._fShaderProgram = gl.createShader(gl.FRAGMENT_SHADER);
579             }
580             gl.shaderSource(this._fShaderProgram, this._fShaderSource);
581             gl.compileShader(this._fShaderProgram);
582             this._updatedFShaderSource = false;
583         },
584         _logShadersInfo: function() {
585             window['console'].log(gl.getShaderInfoLog(this._vShaderProgram));
586             window['console'].log(gl.getShaderInfoLog(this._fShaderProgram));
587         },
588         _getAttributesProperties: function() {
589             var n;
590             n = gl.getProgramParameter(this._program, gl.ACTIVE_ATTRIBUTES);
591             for (var i = 0; i < n; i++) {
592                 var info = gl.getActiveAttrib(this._program, i);
593                 this._attribLocs[info.name] = i;
594                 addAttributesProperty(this, info);
595             }
596         },
597         _getUniformsProperties: function() {
598             var n;
599             n = gl.getProgramParameter(this._program, gl.ACTIVE_UNIFORMS);
600             for (var i = 0; i < n; i++) {
601                 var info = gl.getActiveUniform(this._program, i);
602                 addUniformsProperty(this, info);
603             }
604         },
605         /**
606          * Destroy object.
607          */
608         destroy: function() {
609             gl.deleteProgram(this._vShaderProgram);
610             gl.deleteProgram(this._fShaderProgram);
611             gl.deleteProgram(this._program);
612         }
613     });
614 
615     var addAttributesProperty = function(program, info) {
616         var name = info.name;
617         var loc = program._attribLocs[name];
618         /**
619          * @type {Object}
620          * @memberOf Object.
621          */
622         var desc = {
623             get: function() {
624                 return 'attrib';
625             },
626             set: (function(loc) {
627                 return function(buf) {
628                     gl.enableVertexAttribArray(loc);
629                     buf._setToAttrib(loc);
630                 };
631             }(loc))
632         };
633         Object.defineProperty(program._attributes, name, desc);
634     };
635 
636     var addUniformsProperty = function(program, info) {
637         var name = (info.name.slice(-3) === '[0]') ? info.name.slice(0, -3) : info.name;
638         var loc = gl.getUniformLocation(program._program, info.name);
639         var suffix;
640         var sampler = false;
641         var matrix = false;
642         /**
643          * @type {Object}
644          */
645         var desc = {
646             get: function() {
647                 return 'uniform';
648             }
649         };
650         switch (info.type) {
651             case gl.FLOAT:
652                 suffix = '1f';
653                 break;
654 
655             case gl.FLOAT_MAT2:
656                 matrix = true;
657                 /* falls through */
658             case gl.FLOAT_VEC2:
659                 suffix = '2fv';
660                 break;
661 
662             case gl.FLOAT_MAT3:
663                 matrix = true;
664                 /* falls through */
665             case gl.FLOAT_VEC3:
666                 suffix = '3fv';
667                 break;
668 
669             case gl.FLOAT_MAT4:
670                 matrix = true;
671                 /* falls through */
672             case gl.FLOAT_VEC4:
673                 suffix = '4fv';
674                 break;
675 
676             case gl.SAMPLER_2D:
677             case gl.SAMPLER_CUBE:
678                 sampler = true;
679                 /* falls through */
680             case gl.INT:
681             case gl.BOOL:
682                 suffix = '1i';
683                 break;
684 
685             case gl.INT_VEC2:
686             case gl.BOOL_VEC2:
687                 suffix = '2iv';
688                 break;
689 
690             case gl.INT_VEC3:
691             case gl.BOOL_VEC3:
692                 suffix = '3iv';
693                 break;
694 
695             case gl.INT_VEC4:
696             case gl.BOOL_VEC4:
697                 suffix = '4iv';
698                 break;
699             default:
700                 throw new Error('no match');
701         }
702         if (matrix) {
703             desc.set = (function(loc, suffix) {
704                 return function(value) {
705                     gl['uniformMatrix' + suffix](loc, false, value);
706                 };
707             }(loc, suffix));
708         } else if (sampler) {
709             desc.set = (function(loc, suffix, samplersNum) {
710                 return function(texture) {
711                     gl.activeTexture(gl.TEXTURE0 + samplersNum);
712                     gl.bindTexture(gl.TEXTURE_2D, texture._glTexture);
713                     gl['uniform' + suffix](loc, samplersNum);
714                 };
715             }(loc, suffix, program._samplersNum));
716             program._samplersNum++;
717         } else {
718             desc.set = (function(loc, suffix) {
719                 return function(value) {
720                     gl['uniform' + suffix](loc, value);
721                 };
722             }(loc, suffix));
723         }
724         Object.defineProperty(program._uniforms, name, desc);
725     };
726 
727     /**
728      * @scope enchant.gl.Quat.prototype
729      */
730     enchant.gl.Quat = enchant.Class.create({
731         /**
732          * Class that easily uses quaternions.
733          * @param {Number} x
734          * @param {Number} y
735          * @param {Number} z
736          * @param {Number} rad
737          * @constructs
738          */
739         initialize: function(x, y, z, rad) {
740             var l = Math.sqrt(x * x + y * y + z * z);
741             if (l) {
742                 x /= l;
743                 y /= l;
744                 z /= l;
745             }
746             var s = Math.sin(rad / 2);
747             var w = Math.cos(rad / 2);
748             this._quat = quat4.create([x * s, y * s, z * s, w]);
749         },
750 
751         /**
752          * Performs spherical linear interpolation between quarternions.
753          * Calculates quarternion that supplements rotation between this quarternion and another.
754          * Degree of rotation will be expressed in levels from 0 to 1. 0 is the side of this quarternion, 1 is the side of its counterpart.
755          * New instance will be returned.
756          * @param {enchant.gl.Quat} another Quaternion
757          * @param {Number} ratio
758          * @return {enchant.gl.Quat}
759          */
760         slerp: function(another, ratio) {
761             var q = new enchant.gl.Quat(0, 0, 0, 0);
762             quat4.slerp(this._quat, another._quat, ratio, q._quat);
763             return q;
764         },
765         /**
766          * Performs spherical linear interpolation between quarternions.
767          * Calculates quarternion that supplements rotation between this quarternion and another.
768          * Degree of rotation will be expressed in levels from 0 to 1. 0 is the side of this quarternion, 1 is the side of its counterpart.
769          * This side's value will be overwritten.
770          * @param {enchant.gl.Quat} another Quaternion
771          * @param {Number} ratio
772          * @return {enchant.gl.Quat}
773          */
774         slerpApply: function(another, ratio) {
775             quat4.slerp(this._quat, another._quat, ratio);
776             return this;
777         },
778         /**
779          * Convert quarternion to rotation matrix.
780          * @param {Number[]} matrix
781          * @return {Number[]}
782          */
783         toMat4: function(matrix) {
784             quat4.toMat4(this._quat, matrix);
785             return matrix;
786         },
787         /**
788          * Apply quarternion to vector.
789          * @param {Number[]} vector
790          * @return {Number[]}
791          */
792         multiplyVec3: function(vector) {
793             quat4.multiplyVec3(this._quat, vector);
794             return vector;
795         }
796     });
797 
798     /**
799      * @scope enchant.gl.Light3D.prototype
800      */
801     enchant.gl.Light3D = enchant.Class.create(enchant.EventTarget, {
802         /**
803          * Light source class parent class.
804          *
805          * @constructs
806          * @extends enchant.EventTarget
807          */
808         initialize: function() {
809             this._changedColor = true;
810             this._color = [0.8, 0.8, 0.8];
811         },
812 
813         /**
814          * Light source light color
815          * @type Number[]
816          */
817         color: {
818             set: function(array) {
819                 this._color = array;
820                 this._changedColor = true;
821             },
822             get: function() {
823                 return this._color;
824             }
825         }
826     });
827 
828     /**
829      * @scope enchant.gl.AmbientLight.prototype
830      */
831     enchant.gl.AmbientLight = enchant.Class.create(enchant.gl.Light3D, {
832         /**
833          * Class for setting light source in 3D scene.
834          * Environmental Light.
835          * @example
836          *   var scene = new Scene3D();
837          *   var light = new AmbientLight();
838          *   light.color = [1.0, 1.0, 0.0];
839          *   light.directionY = 10;
840          *   scene.setAmbientLight(light);
841          *
842          * @constructs
843          * @extends enchant.gl.Light3D
844          */
845         initialize: function() {
846             enchant.gl.Light3D.call(this);
847         }
848     });
849 
850     /**
851      * @scope enchant.gl.DirectionalLight.prototype
852      */
853     enchant.gl.DirectionalLight = enchant.Class.create(enchant.gl.Light3D, {
854         /**
855          * Class for setting light source in 3D scene.
856          * Directioned light source without position.
857          * @example
858          *   var scene = new Scene3D();
859          *   var light = new DirectionalLight();
860          *   light.color = [1.0, 1.0, 0.0];
861          *   light.directionY = 10;
862          *   scene.setDirectionalLight(light);
863          *
864          * @constructs
865          * @extends enchant.gl.Light3D
866          */
867         initialize: function() {
868             enchant.gl.Light3D.call(this);
869             this._directionX = 0.5;
870             this._directionY = 0.5;
871             this._directionZ = 1.0;
872             this._changedDirection = true;
873         }
874     });
875 
876     /**
877      * Light source exposure direction x component
878      * @type Number
879      */
880     enchant.gl.DirectionalLight.prototype.directionX = 0.5;
881 
882     /**
883      * Light source exposure direction y component
884      * @type Number
885      */
886     enchant.gl.DirectionalLight.prototype.directionY = 0.5;
887 
888     /**
889      * Light source exposure direction z component
890      * @type Number
891      */
892     enchant.gl.DirectionalLight.prototype.directionZ = 1.0;
893 
894     /**
895      * Light source exposure direction
896      * @type {Number}
897      */
898     'directionX directionY directionZ'.split(' ').forEach(function(prop) {
899         Object.defineProperty(enchant.gl.DirectionalLight.prototype, prop, {
900             get: function() {
901                 return this['_' + prop];
902             },
903             set: function(n) {
904                 this['_' + prop] = n;
905                 this._changedDirection = true;
906             }
907         });
908         enchant.gl.DirectionalLight.prototype[prop] = 0;
909     });
910 
911     /**
912      * @scope enchant.gl.PointLight.prototype
913      */
914     enchant.gl.PointLight = enchant.Class.create(enchant.gl.Light3D, {
915         /**
916          * Class that sets 3D scene light source.
917          * Directionless positional light source.
918          * At present, will not be applied even if added to scene.
919          * @example
920          *   var scene = new Scene3D();
921          *   var light = new PointLight();
922          *   light.color = [1.0, 1.0, 0.0];
923          *   light.y = 10;
924          *   scene.addLight(light);
925          *
926          * @constructs
927          * @extends enchant.gl.Light3D
928          */
929         initialize: function() {
930             enchant.gl.Light3D.call(this);
931             this._x = 0;
932             this._y = 0;
933             this._z = 0;
934             this._changedPosition = true;
935         }
936     });
937 
938     /**
939      * Light source x axis
940      * @type Number
941      */
942     enchant.gl.PointLight.prototype.x = 0;
943 
944     /**
945      * Light source y axis
946      * @type Number
947      */
948     enchant.gl.PointLight.prototype.y = 0;
949 
950     /**
951      * Light source z axis
952      * @type Number
953      */
954     enchant.gl.PointLight.prototype.z = 0;
955 
956     'x y z'.split(' ').forEach(function(prop) {
957         Object.defineProperty(enchant.gl.PointLight.prototype, prop, {
958             get: function() {
959                 return this['_' + prop];
960             },
961             set: function(n) {
962                 this['_' + prop] = n;
963                 this._changedPosition = true;
964             }
965         });
966         enchant.gl.PointLight.prototype[prop] = 0;
967     });
968 
969 
970     /**
971      * @scope enchant.gl.Texture.prototype
972      */
973     enchant.gl.Texture = enchant.Class.create({
974         /**
975          * Class to store Sprite3D texture information.
976          * @example
977          *   var sprite = new Sprite3D();
978          *   var texture = new Texture();
979          *   texture.src = "http://example.com/texture.png";
980          *   // Can also be declared as below.
981          *   // var texture = new Texture("http://example.com/texture.png");
982          *   sprite.texture = texture;
983          * @constructs
984          */
985         initialize: function(src, opt) {
986             /**
987              * Ambient light parameter
988              * @type Number[]
989              */
990             this.ambient = [ 0.1, 0.1, 0.1, 1.0 ];
991 
992             /**
993              * Light scattering parameter
994              * @type Number[]
995              */
996             this.diffuse = [ 1.0, 1.0, 1.0, 1.0];
997 
998             /**
999              * Amount of light reflection
1000              * @type Number[]
1001              */
1002             this.specular = [ 1.0, 1.0, 1.0, 1.0 ];
1003 
1004             /**
1005              * Amount of luminescence
1006              * @type Number[]
1007              */
1008             this.emission = [ 0.0, 0.0, 0.0, 1.0 ];
1009 
1010             /**
1011              * Specular figures
1012              * @type Number
1013              */
1014             this.shininess = 20;
1015 
1016             this._glTexture = null;
1017             this._image = null;
1018             this._wrap = 10497;
1019             this._mipmap = false;
1020             this._flipY = true;
1021             if (opt) {
1022                 var valid = ['flipY', 'wrap', 'mipmap'];
1023                 for (var prop in opt) {
1024                     if (opt.hasOwnProperty(prop)) {
1025                         if (valid.indexOf(prop) !== -1) {
1026                             this['_' + prop] = opt[prop];
1027                         }
1028                     }
1029                 }
1030             }
1031             if (src) {
1032                 this.src = src;
1033             }
1034         },
1035 
1036         _write: function() {
1037             gl.bindTexture(gl.TEXTURE_2D, this._glTexture);
1038             enchant.Core.instance.GL.textureManager._writeWebGLTexture(this._image, gl.TEXTURE_2D, this._wrap, this._mipmap);
1039             gl.bindTexture(gl.TEXTURE_2D, null);
1040         },
1041 
1042         /**
1043          * Texture image source.
1044          * You can set URL or core.assets data.
1045          * @type String
1046          * @type enchant.Surface
1047          */
1048         src: {
1049             get: function() {
1050                 return this._src;
1051             },
1052             set: function(source) {
1053                 if (typeof source === 'undefined' ||
1054                     source === null) {
1055                     return;
1056                 }
1057                 var that = this;
1058                 var core = enchant.Core.instance;
1059                 var onload = (function(that) {
1060                     return function() {
1061                         that._glTexture = core.GL.textureManager.getWebGLTexture(that._image, that._flipY, that._wrap, that._mipmap);
1062                     };
1063                 }(that));
1064                 if (source instanceof Image) {
1065                     this._image = source;
1066                     onload();
1067                 } else if (source instanceof enchant.Surface) {
1068                     this._image = source._element;
1069                     onload();
1070                 } else if (typeof source === 'string') {
1071                     this._image = new Image();
1072                     this._image.onload = onload;
1073                     this._image.src = source;
1074                 } else {
1075                     this._image = source;
1076                     this._image.src = "c" + Math.random();
1077                     onload();
1078                 }
1079             }
1080         }
1081     });
1082 
1083     /**
1084      * @scope enchant.gl.Buffer.prototype
1085      */
1086     enchant.gl.Buffer = enchant.Class.create({
1087         /**
1088          * Controls peak and other array information.
1089          * Used as enchant.gl.Mesh property.
1090          * @param {*} params parameter
1091          * @param {Number[]} array
1092          *
1093          * @example
1094          * var index = [ 0, 1, 2, 2, 3, 0 ];
1095          * var indices = new Buffer(Buffer.INDICES, index);
1096          *
1097          * @constructs
1098          */
1099         initialize: function(params, array) {
1100             this._setParams(params);
1101             if (typeof array !== 'undefined') {
1102                 this._array = array;
1103             } else {
1104                 this._array = [];
1105             }
1106             this._buffer = null;
1107         },
1108         /**
1109          * Bind buffer.
1110          */
1111         bind: function() {
1112             gl.bindBuffer(this.btype, this._buffer);
1113         },
1114         /**
1115          * Unbind buffer.
1116          */
1117         unbind: function() {
1118             gl.bindBuffer(this.btype, null);
1119         },
1120         _setParams: function(params) {
1121             for (var prop in params) {
1122                 if (params.hasOwnProperty(prop)) {
1123                     this[prop] = params[prop];
1124                 }
1125             }
1126         },
1127         _create: function() {
1128             this._buffer = gl.createBuffer();
1129         },
1130         _delete: function() {
1131             gl.deleteBuffer(this._buffer);
1132         },
1133         _bufferData: function() {
1134             this.bind();
1135             gl.bufferData(this.btype, new this.Atype(this._array), gl.STATIC_DRAW);
1136             this.unbind();
1137         },
1138         _bufferDataFast: function() {
1139             this.bind();
1140             gl.bufferData(this.btype, this._array, gl.STATIC_DRAW);
1141             this.unbind();
1142         },
1143         _setToAttrib: function(loc) {
1144             this.bind();
1145             gl.vertexAttribPointer(loc, this.size, this.type, this.norm, this.stride, this.offset);
1146             this.unbind();
1147         },
1148         /**
1149          * Destroy object.
1150          */
1151         destroy: function() {
1152             this._delete();
1153         }
1154     });
1155 
1156     var bufferProto = Object.getPrototypeOf(enchant.gl.Buffer);
1157     bufferProto.VERTICES = bufferProto.NORMALS = {
1158         size: 3,
1159         type: 5126,
1160         norm: false,
1161         stride: 0,
1162         offset: 0,
1163         btype: 34962,
1164         Atype: Float32Array
1165     };
1166     bufferProto.TEXCOORDS = {
1167         size: 2,
1168         type: 5126,
1169         normed: false,
1170         stride: 0,
1171         ptr: 0,
1172         btype: 34962,
1173         Atype: Float32Array
1174     };
1175     bufferProto.COLORS = {
1176         size: 4,
1177         type: 5126,
1178         normed: false,
1179         stride: 0,
1180         ptr: 0,
1181         btype: 34962,
1182         Atype: Float32Array
1183     };
1184     bufferProto.INDICES = {
1185         size: 3,
1186         type: 5123,
1187         normed: false,
1188         stride: 0,
1189         offset: 0,
1190         btype: 34963,
1191         Atype: Uint16Array
1192     };
1193 
1194     /**
1195      * @scope enchant.gl.Mesh.prototype
1196      */
1197     enchant.gl.Mesh = enchant.Class.create({
1198         /**
1199          * Class to store peak arrays and textures.
1200          * Used as a sprite property.
1201          * @constructs
1202          */
1203         initialize: function() {
1204             this.__count = 0;
1205             this._appear = false;
1206             this._vertices = new enchant.gl.Buffer(enchant.gl.Buffer.VERTICES);
1207             this._normals = new enchant.gl.Buffer(enchant.gl.Buffer.NORMALS);
1208             this._colors = new enchant.gl.Buffer(enchant.gl.Buffer.COLORS);
1209             this._texCoords = new enchant.gl.Buffer(enchant.gl.Buffer.TEXCOORDS);
1210             this._indices = new enchant.gl.Buffer(enchant.gl.Buffer.INDICES);
1211             this.texture = new enchant.gl.Texture();
1212         },
1213         /**
1214          * Change Mesh color.
1215          * Becomes peak array for Mesh.colors set color.
1216          * @param {Number[]|String} color z Amount of parallel displacement on z axis
1217          * @example
1218          *   var sprite = new Sprite3D();
1219          *   //Sets to purple. All yield the same result.
1220          *   sprite.mesh.setBaseColor([1.0, 0.0, 1.0, 0.0]);
1221          *   sprite.mesh.setBaseColor('#ff00ff');
1222          *   sprite.mesh.setBaseColor('rgb(255, 0, 255');
1223          *   sprite.mesh.setBaseColor('rgba(255, 0, 255, 1.0');
1224          */
1225         setBaseColor: function(color) {
1226             var c = enchant.Core.instance.GL.parseColor(color);
1227             var newColors = [];
1228             for (var i = 0, l = this.vertices.length / 3; i < l; i++) {
1229                 Array.prototype.push.apply(newColors, c);
1230             }
1231             this.colors = newColors;
1232         },
1233         /**
1234          * Reverse direction of the mesh surface and the normal vector.
1235          */
1236         reverse: function() {
1237             var norm = this.normals;
1238             var idx = this.indices;
1239             var t, i, l;
1240             for (i = 0, l = norm.length; i < l; i++) {
1241                 norm[i] *= -1;
1242             }
1243             for (i = 0, l = idx.length; i < l; i += 3) {
1244                 t = idx[i + 1];
1245                 idx[i + 1] = idx[i + 2];
1246                 idx[i + 2] = t;
1247             }
1248             this._normals._bufferData();
1249             this._indices._bufferData();
1250         },
1251         _createBuffer: function() {
1252             for (var prop in this) {
1253                 if (this.hasOwnProperty(prop)) {
1254                     if (this[prop] instanceof enchant.gl.Buffer) {
1255                         this[prop]._create();
1256                         this[prop]._bufferData();
1257                     }
1258                 }
1259             }
1260         },
1261         _deleteBuffer: function() {
1262             for (var prop in this) {
1263                 if (this.hasOwnProperty(prop)) {
1264                     if (this[prop] instanceof enchant.gl.Buffer) {
1265                         this[prop]._delete();
1266                     }
1267                 }
1268             }
1269         },
1270         _controlBuffer: function() {
1271             if (this._appear) {
1272                 if (this.__count <= 0) {
1273                     this._appear = false;
1274                     this._deleteBuffer();
1275                 }
1276             } else {
1277                 if (this.__count > 0) {
1278                     this._appear = true;
1279                     this._createBuffer();
1280                 }
1281             }
1282         },
1283         /**
1284          * @type {Number}
1285          */
1286         _count: {
1287             get: function() {
1288                 return this.__count;
1289             },
1290             set: function(c) {
1291                 this.__count = c;
1292                 this._controlBuffer();
1293             }
1294         },
1295         _join: function(another, ox, oy, oz) {
1296             var triangles = this.vertices.length / 3,
1297                 vertices = this.vertices.slice(0),
1298                 i, l;
1299             for (i = 0, l = another.vertices.length; i < l; i += 3) {
1300                 vertices.push(another.vertices[i] + ox);
1301                 vertices.push(another.vertices[i + 1] + oy);
1302                 vertices.push(another.vertices[i + 2] + oz);
1303             }
1304             this.vertices = vertices;
1305             this.normals = this.normals.concat(another.normals);
1306             this.texCoords = this.texCoords.concat(another.texCoords);
1307             this.colors = this.colors.concat(another.colors);
1308             var indices = this.indices.slice(0);
1309             for (i = 0, l = another.indices.length; i < l; i++) {
1310                 indices.push(another.indices[i] + triangles);
1311             }
1312             this.indices = indices;
1313         },
1314         /**
1315          * Destroy object.
1316          */
1317         destroy: function() {
1318             this._deleteBuffer();
1319         }
1320     });
1321 
1322     /**
1323      * Mesh peak array.
1324      * Sets 3 elements together at peak. The complete number of elements becomes 3n corresponding to the quantity of the peak.
1325      * The 3n, 3n+1, and 3n+2 elements become, respectively, the n peak x, y, and z coordinates.
1326      * @example
1327      *   var sprite = new Sprite3D();
1328      *   //Substitute peak array
1329      *   //Data is stored in an order of x, y, z, x, y, z...
1330      *   sprite.mesh.vertices = [
1331      *       0.0, 0.0, 0.0,  //0 peak (0.0, 0.0, 0.0)
1332      *       1.0, 0.0, 0.0,  //1 peak (1.0, 0.0, 0.0)
1333      *       1.0, 1.0, 0.0,  //2 peak (1.0, 1.0, 0.0)
1334      *       0.0, 1.0, 0.0   //3 peak (0.0, 1.0, 0.0)
1335      *   ];
1336      * @type Number[]
1337      * @see enchant.gl.Mesh#indices
1338      * @see enchant.gl.Mesh#normals
1339      * @see enchant.gl.Mesh#texCoords
1340      */
1341     enchant.gl.Mesh.prototype.vertices = [];
1342 
1343     /**
1344      * Mesh peak normal vector array.
1345      * Sets 3 elements as one in normal vector. The complete element number becomes 3n for normal vector quantity n.
1346      * 3n, 3n+1, and 3n+2 elements are the n level peak x, y, and z elements of the normal vector.
1347      * The normal vector is used in calculations for lighting and shadow.
1348      * @example
1349      *   var sprite = new Sprite3D();
1350      *   //Substitutes peak array
1351      *   //Data is stored in an order of x, y, z, x, y, z...
1352      *   sprite.vertices = [
1353      *       0.0, 0.0, 0.0,  //0 peak (0.0, 0.0, 0.0)
1354      *       1.0, 0.0, 0.0,  //1 peak (1.0, 0.0, 0.0)
1355      *       1.0, 1.0, 0.0,  //2 peak (1.0, 1.0, 0.0)
1356      *       0.0, 1.0, 0.0   //3 peak (0.0, 1.0, 0.0)
1357      *   ];
1358      *
1359      *   //Substitutes normal vector array
1360      *   //Data is a stored in an order of x, y, z, x, y, z...
1361      *   sprite.normals = [
1362      *       0.0, 0.0, 0.0,  //0 level peak normal vector (0.0, 0.0, 0.0)
1363      *       1.0, 0.0, 0.0,  //1 level peak normal vector (1.0, 0.0, 0.0)
1364      *       1.0, 1.0, 0.0,  //2 level peak normal vector (1.0, 1.0, 0.0)
1365      *       0.0, 1.0, 0.0   //3 level peak normal vector (0.0, 1.0, 0.0)
1366      *   ];
1367      * @type Number[]
1368      * @see enchant.gl.Mesh#vertices
1369      * @see enchant.gl.Mesh#indices
1370      * @see enchant.gl.Mesh#texCoords
1371      */
1372     enchant.gl.Mesh.prototype.normals = [];
1373 
1374     /**
1375      * Mesh texture mapping array.
1376      * Sets two elements as one in uv coordinates. The total number of elements becomes 2n in response to the peak quantity.
1377      * 2n, 2n+1 level elements correspond to n level peak texture u, v coordinates.
1378      * The coordinates that can be acquired for each coordinate correspond to 0<=u, v<=1.
1379      * @example
1380      *   var sprite = new Sprite3D();
1381      *   var texture = new Texture();
1382      *   texture.src = "texture.png";
1383      *   sprite.mesh.texture = texture;
1384      *
1385      *   //Substitutes peak level
1386      *   //Data is stored in an order of x, y, z, x, y, z...
1387      *   sprite.vertices = [
1388      *       0.0, 0.0, 0.0,  //0 peak (0.0, 0.0, 0.0)
1389      *       1.0, 0.0, 0.0,  //1 peak (1.0, 0.0, 0.0)
1390      *       1.0, 1.0, 0.0,  //2 peak (1.0, 1.0, 0.0)
1391      *       0.0, 1.0, 0.0   //3 peak (0.0, 1.0, 0.0)
1392      *   ];
1393      *
1394      *   //Substitutes uv coordinate array
1395      *   //Data is stored in an order of u, v, u, v...
1396      *   sprite.texCoords = [
1397      *       0.0, 0.0,  //0番目の頂点のuv座標(0.0, 0.0)
1398      *       1.0, 0.0,  //1番目の頂点のuv座標(1.0, 0.0)
1399      *       1.0, 1.0,  //2番目の頂点のuv座標(1.0, 1.0)
1400      *       0.0, 1.0   //3番目の頂点のuv座標(0.0, 1.0)
1401      *   ];
1402      * @type Number[]
1403      * @see enchant.gl.Mesh#vertices
1404      * @see enchant.gl.Mesh#indices
1405      * @see enchant.gl.Mesh#normals
1406      * @see enchant.gl.Mesh#texture#
1407      */
1408     enchant.gl.Mesh.prototype.texCoords = [];
1409 
1410     /**
1411      * Sprite3D peak index array.
1412      * 3 elements are set as one in a triangle. The total number of elements becomes 3n corresponding to the triangle's total quantity n.
1413      * Index level is the peak number designated in {@link enchant.gl.Sprite3D#vertices}.
1414      * @example
1415      *   var sprite = new Sprite3D();
1416      *   //Substitutes peak array
1417      *   //Data is stored in an order of x, y, z, x, y, z...
1418      *   sprite.vertices = [
1419      *       0.0, 0.0, 0.0,  //0 peak (0.0, 0.0, 0.0)
1420      *       1.0, 0.0, 0.0,  //1 peak (1.0, 0.0, 0.0)
1421      *       1.0, 1.0, 0.0,  //2 peak (1.0, 1.0, 0.0)
1422      *       0.0, 1.0, 0.0   //3 peak (0.0, 1.0, 0.0)
1423      *   ];
1424      *
1425      *   //Substitutes peak index
1426      *   //Draws triangle with 3 elements as one
1427      *   //In this example the two triangles (0,0,0), (1,0,0), (1,1,0) and
1428      *   //(1,1,0), (0,1,0), (0,0,0) are drawn.
1429      *   sprite.indices = [
1430      *       0, 1, 2,
1431      *       2, 3, 0
1432      *   ];
1433      *   var scene = new Scene3D();
1434      *   scene.addChild(sprite);
1435      * @type Integer[]
1436      * @see enchant.gl.Mesh#vertices
1437      * @see enchant.gl.Mesh#normals
1438      * @see enchant.gl.Mesh#texCoords
1439      */
1440     enchant.gl.Mesh.prototype.indices = [];
1441 
1442     /**
1443      * Mesh peak color array.
1444      * The 4 elements are set as one peak color. The entire number of elements becomes 4n corresponding to the peak quantity n.
1445      * The 4n, 4n+1, 4n+2, and 4n+3 elements are the n level peak colors r, g, b, a.
1446      * Peak color is used for drawing when Texture is not assigned to Sprite3D Texture.
1447      * {@link enchant.gl.Mesh#setBaseColor} can be used to bundle together and change.
1448      * @example
1449      *   var sprite = new Sprite3D();
1450      *   //Substitutes peak array
1451      *   //Data is stored in an order of x, y, z, x, y, z...
1452      *   sprite.vertices = [
1453      *       0.0, 0.0, 0.0,  //0 peak (0.0, 0.0, 0.0)
1454      *       1.0, 0.0, 0.0,  //1 peak (1.0, 0.0, 0.0)
1455      *       1.0, 1.0, 0.0,  //2 peak (1.0, 1.0, 0.0)
1456      *       0.0, 1.0, 0.0   //3 peak (0.0, 1.0, 0.0)
1457      *   ];
1458      *
1459      *   //Substitutes peak level array
1460      *   //Data is stored in an order of r, g, b, a, r, g, b, a...
1461      *   sprite.normals = [
1462      *       0.0, 0.0, 1.0, 1.0, //0 peak color (0.0, 0.0, 1.0, 1.0)
1463      *       0.0, 1.0, 0.0, 1.0, //1 peak color (0.0, 1.0, 0.0, 1.0)
1464      *       0.0, 1.0, 1.0, 1.0, //2 peak color (0.0, 1.0, 1.0, 1.0)
1465      *       1.0, 0.0, 0.0, 1.0  //3 peak color (1.0, 0.0, 0.0, 1.0)
1466      *   ];
1467      * @type Number[]
1468      * @see enchant.gl.Mesh#setBaseColor
1469      */
1470     enchant.gl.Mesh.prototype.colors = [];
1471 
1472     'vertices normals colors texCoords indices'.split(' ').forEach(function(prop) {
1473         Object.defineProperty(enchant.gl.Mesh.prototype, prop, {
1474             get: function() {
1475                 return this['_' + prop]._array;
1476             },
1477             set: function(array) {
1478                 this['_' + prop]._array = array;
1479                 if (this._appear) {
1480                     this['_' + prop]._bufferData();
1481                 }
1482             }
1483         });
1484     });
1485 
1486     /**
1487      * @scope enchant.gl.Sprite3D.prototype
1488      */
1489     enchant.gl.Sprite3D = enchant.Class.create(enchant.EventTarget, {
1490         /**
1491          * Class with Sprite3D display function.
1492          * <p>By adding {@link enchant.gl.Scene3D} instance, you can display atop an image.
1493          * By changing {@link enchant.gl.Sprite3D#vertices}, {@link enchant.gl.Sprite3D#indices},
1494          * {@link enchant.gl.Sprite3D#normals} and others, you can freely draw Sprite3D,
1495          * as well as pasting texture and more.</p>
1496          * <p>In addition, it is also possible to add Sprite3D as a child, and all child classes will be drawn with coordinates based on their parents.</p>
1497          * @example
1498          *   //Scene initialization
1499          *   var scene = new Scene3D();
1500          *   //Sprite3D initialization
1501          *   var sprite = new Sprite3D();
1502          *   //Add Sprite3D to scene
1503          *   scene.addChild(sprite);
1504          * @constructs
1505          * @extends enchant.EventTarget
1506          */
1507         initialize: function() {
1508             enchant.EventTarget.call(this);
1509 
1510             /**
1511              * Array for child Sprite 3D element.
1512              * Can acquire list of Sprite3Ds added as child classes to this element.
1513              * When adding or subtracting child classes, without directly operating this array,
1514              * {@link enchant.gl.Sprite3D#addChild} or {@link enchant.gl.Sprite3D#removeChild} is used.
1515              * @type enchant.gl.Sprite3D[]
1516              * @see enchant.gl.Sprite3D#addChild
1517              * @see enchant.gl.Sprite3D#removeChild
1518              */
1519             this.childNodes = [];
1520 
1521             /**
1522              * The scene object currently added by this Sprite3D.
1523              * When no scene is added this is null.
1524              * @type enchant.gl.Scene3D
1525              * @see enchant.gl.Scene3D#addChild
1526              */
1527             this.scene = null;
1528 
1529             /**
1530              * Sprite3D parent element.
1531              * When no parent exists this is null.
1532              * @type enchant.gl.Sprite3D|enchant.gl.Scene3D
1533              */
1534             this.parentNode = null;
1535 
1536             /**
1537              * Mesh object applied to Sprite3D.
1538              * @type enchant.gl.Mesh
1539              * @example
1540              *   var sprite = new Sprite3D();
1541              *   sprite.mesh = new Mesh();
1542              */
1543             this._mesh = null;
1544 
1545             this.program = null;
1546 
1547             this.bounding = new enchant.gl.collision.BS();
1548             this.bounding.parent = this;
1549 
1550             this.age = 0;
1551 
1552             this._x = 0;
1553             this._y = 0;
1554             this._z = 0;
1555             this._scaleX = 1;
1556             this._scaleY = 1;
1557             this._scaleZ = 1;
1558             this._changedTranslation = true;
1559             this._changedRotation = true;
1560             this._changedScale = true;
1561             this._touchable = true;
1562 
1563             this._global = vec3.create();
1564             this.globalX = 0;
1565             this.globalY = 0;
1566             this.globalZ = 0;
1567 
1568             this._matrix = mat4.identity();
1569             this.tmpMat = mat4.identity();
1570             this.modelMat = mat4.identity();
1571             this._rotation = mat4.identity();
1572             this._normMat = mat3.identity();
1573 
1574             var core = enchant.Core.instance;
1575             this.detectColor = core.GL.detectColorManager.attachDetectColor(this);
1576 
1577             var parentEvent = function(e) {
1578                 if (this.parentNode instanceof enchant.gl.Sprite3D) {
1579                     this.parentNode.dispatchEvent(e);
1580                 }
1581             };
1582             this.addEventListener('touchstart', parentEvent);
1583             this.addEventListener('touchmove', parentEvent);
1584             this.addEventListener('touchend', parentEvent);
1585 
1586             var added = function(e) {
1587                 if (this.mesh !== null) {
1588                     this.mesh._count++;
1589                 }
1590                 if (this.childNodes.length) {
1591                     for (var i = 0, l = this.childNodes.length; i < l; i++) {
1592                         this.childNodes[i].scene = this.scene;
1593                         this.childNodes[i].dispatchEvent(e);
1594                     }
1595                 }
1596             };
1597             this.addEventListener('addedtoscene', added);
1598 
1599             var removed = function(e) {
1600                 if (this.mesh !== null) {
1601                     this.mesh._count--;
1602                 }
1603                 if (this.childNodes.length) {
1604                     for (var i = 0, l = this.childNodes.length; i < l; i++) {
1605                         this.childNodes[i].scene = null;
1606                         this.childNodes[i].dispatchEvent(e);
1607                     }
1608                 }
1609             };
1610             this.addEventListener('removedfromscene', removed);
1611 
1612         },
1613 
1614         /**
1615          * Executes the reproduction of Sprite3D.
1616          * Position, rotation line, and others will be returned to a copied, new instance.
1617          * @example
1618          *   var sp = new Sprite3D();
1619          *   sp.x = 15;
1620          *   var sp2 = sp.clone();
1621          *   //sp2.x = 15;
1622          * @return {enchant.gl.Sprite3D}
1623          */
1624         clone: function() {
1625             var clone = new enchant.gl.Sprite3D();
1626             for (var prop in this) {
1627                 if (typeof this[prop] === 'number' ||
1628                     typeof this[prop] === 'string') {
1629                     clone[prop] = this[prop];
1630                 } else if (this[prop] instanceof WebGLBuffer) {
1631                     clone[prop] = this[prop];
1632                 } else if (this[prop] instanceof Float32Array) {
1633                     clone[prop] = new Float32Array(this[prop]);
1634                 } else if (this[prop] instanceof Array &&
1635                     prop !== 'childNodes' &&
1636                     prop !== 'detectColor') {
1637                     clone[prop] = this[prop].slice(0);
1638                 }
1639             }
1640             if (this.mesh !== null) {
1641                 clone.mesh = this.mesh;
1642             }
1643             if (this.childNodes) {
1644                 for (var i = 0, l = this.childNodes.length; i < l; i++) {
1645                     clone.addChild(this.childNodes[i].clone());
1646                 }
1647             }
1648             return clone;
1649         },
1650 
1651         /**
1652          * Sets condition of other Sprite3D.
1653          * Can be used corresponding Collada file's loaded assets.
1654          * @example
1655          *   var sp = new Sprite3D();
1656          *   sp.set(core.assets['sample.dae']);
1657          *   //Becomes Sprite3D with sample.dae model information
1658          *
1659          */
1660         set: function(sprite) {
1661             for (var prop in sprite) {
1662                 if (typeof sprite[prop] === 'number' ||
1663                     typeof sprite[prop] === 'string') {
1664                     this[prop] = sprite[prop];
1665                 } else if (sprite[prop] instanceof WebGLBuffer) {
1666                     this[prop] = sprite[prop];
1667                 } else if (sprite[prop] instanceof Float32Array) {
1668                     this[prop] = new Float32Array(sprite[prop]);
1669                 } else if (sprite[prop] instanceof Array &&
1670                     prop !== 'childNodes' &&
1671                     prop !== 'detectColor') {
1672                     this[prop] = sprite[prop].slice(0);
1673                 }
1674             }
1675             if (sprite.mesh !== null) {
1676                 this.mesh = sprite.mesh;
1677             }
1678             if (sprite.childNodes) {
1679                 for (var i = 0, l = sprite.childNodes.length; i < l; i++) {
1680                     this.addChild(sprite.childNodes[i].clone());
1681                 }
1682             }
1683         },
1684 
1685         /**
1686          * Add child Sprite3D.
1687          * When it is added, an "added" event will be created for child Sprite3D.
1688          * When a parent is already added to scene, it will be added to scene,
1689          * and an "addedtoscene" event will be created.
1690          * Child Sprite3D for adding @param {enchant.gl.Sprite3D} sprite.
1691          * @example
1692          *   var parent = new Sprite3D();
1693          *   var child = new Sprite3D();
1694          *   //Add Sprite3D as child to another Sprite3D
1695          *   parent.addChild(child);
1696          * @see enchant.gl.Sprite3D#removeChild
1697          * @see enchant.gl.Sprite3D#childNodes
1698          * @see enchant.gl.Sprite3D#parentNode
1699          */
1700         addChild: function(sprite) {
1701             this.childNodes.push(sprite);
1702             sprite.parentNode = this;
1703             sprite.dispatchEvent(new enchant.Event('added'));
1704             if (this.scene) {
1705                 sprite.scene = this.scene;
1706                 sprite.dispatchEvent(new enchant.Event('addedtoscene'));
1707             }
1708         },
1709 
1710         /**
1711          * Deletes designated child Sprite3D.
1712          * When deletion is complete, a "removed" event will be created for child Sprite3D.
1713          * When added to scene, it will be deleted from that scene,
1714          * and a "removedfromscene" event will be created.
1715          * Child Sprite3D for deleting @param {enchant.gl.Sprite3D} sprite.
1716          * @example
1717          *   var scene = new Scene3D();
1718          *   //Deletes scene's first child
1719          *   scene.removeChild(scene.childNodes[0]);
1720          * @see enchant.gl.Sprite3D#addChild
1721          * @see enchant.gl.Sprite3D#childNodes
1722          * @see enchant.gl.Sprite3D#parentNode
1723          */
1724         removeChild: function(sprite) {
1725             var i;
1726             if ((i = this.childNodes.indexOf(sprite)) !== -1) {
1727                 this.childNodes.splice(i, 1);
1728                 sprite.parentNode = null;
1729                 sprite.dispatchEvent(new enchant.Event('removed'));
1730                 if (this.scene) {
1731                     sprite.scene = null;
1732                     sprite.dispatchEvent(new enchant.Event('removedfromscene'));
1733                 }
1734             }
1735         },
1736 
1737 
1738         /**
1739          * Other object collison detection.
1740          * Can detect collisions with collision detection objects with x, y, z properties.
1741          * @param {enchant.gl.Sprite3D} bounding Target object
1742          * @return {Boolean}
1743          */
1744         intersect: function(another) {
1745             return this.bounding.intersect(another.bounding);
1746         },
1747 
1748         /**
1749          * Parallel displacement of Sprite3D.
1750          * Displaces each coordinate a designated amount from its current location.
1751          * @param {Number} x Parallel displacement of x axis
1752          * @param {Number} y Parallel displacement of y axis
1753          * @param {Number} z Parallel displacement of z axis
1754          * @example
1755          *   var sprite = new Sprite3D();
1756          *   //Parallel displacement by 10 along the x axis, 3 along the y axis, and -20 along the z axis
1757          *   sprite.translate(10, 3, -20);
1758          * @see enchant.gl.Sprite3D#x
1759          * @see enchant.gl.Sprite3D#y
1760          * @see enchant.gl.Sprite3D#z
1761          * @see enchant.gl.Sprite3D#scale
1762          */
1763         translate: function(x, y, z) {
1764             this._x += x;
1765             this._y += y;
1766             this._z += z;
1767             this._changedTranslation = true;
1768         },
1769 
1770         /**
1771          * Moves forward Sprite3D.
1772          * @param {Number} speed
1773          */
1774         forward: function(speed) {
1775             var x = this._rotation[8] * speed;
1776             var y = this._rotation[9] * speed;
1777             var z = this._rotation[10] * speed;
1778             this.translate(x, y, z);
1779         },
1780 
1781         /**
1782          * Moves side Sprite3D.
1783          * @param {Number} speed
1784          */
1785         sidestep: function(speed) {
1786             var x = this._rotation[0] * speed;
1787             var y = this._rotation[1] * speed;
1788             var z = this._rotation[2] * speed;
1789             this.translate(x, y, z);
1790         },
1791 
1792         /**
1793          * Moves up Sprite3D.
1794          * @param {Number} speed
1795          */
1796         altitude: function(speed) {
1797             var x = this._rotation[4] * speed;
1798             var y = this._rotation[5] * speed;
1799             var z = this._rotation[6] * speed;
1800             this.translate(x, y, z);
1801         },
1802 
1803         /**
1804          * Expand or contract Sprite3D.
1805          * Expands each axis by a designated expansion rate.
1806          * @param {Number} x x axis expansion rate
1807          * @param {Number} y y axis expansion rate
1808          * @param {Number} z z axis expansion rate
1809          * @example
1810          *   var sprite = new Sprite3D();
1811          *   //Expand x axis by 2.0 times, y axis by 3.0 times, and z axis by 0.5 times
1812          *   sprite.scale(2,0, 3.0, 0.5);
1813          * @see enchant.gl.Sprite3D#scaleX
1814          * @see enchant.gl.Sprite3D#scaleY
1815          * @see enchant.gl.Sprite3D#scaleZ
1816          * @see enchant.gl.Sprite3D#translate
1817          */
1818         scale: function(x, y, z) {
1819             this._scaleX *= x;
1820             this._scaleY *= y;
1821             this._scaleZ *= z;
1822             this._changedScale = true;
1823         },
1824 
1825         /**
1826          * Sprite3D name
1827          * @type String
1828          */
1829         name: {
1830             get: function() {
1831                 return this._name;
1832             },
1833             set: function(name) {
1834                 this._name = name;
1835             }
1836         },
1837 
1838         /**
1839          * Sprite3D rotation line.
1840          * Array is a one-dimensional array of length 16, interpreted as the 4x4 line destination.
1841          * @example
1842          *   var sprite = new Sprite3D();
1843          *   //45 degree rotation along the x axis
1844          *   var rotX = Math.PI() / 4;
1845          *   sprite.rotation = [
1846          *       1, 0, 0, 0,
1847          *       0, Math.cos(rotX), -Math.sin(rotX), 0,
1848          *       0, Math.sin(rotX), Math.cos(rotX), 0,
1849          *       0, 0, 0, 1
1850          *   ];
1851          * @type Number[]
1852          */
1853         rotation: {
1854             get: function() {
1855                 return this._rotation;
1856             },
1857             set: function(rotation) {
1858                 this._rotation = rotation;
1859                 this._changedRotation = true;
1860             }
1861         },
1862 
1863         /**
1864          * Sets rotation line in rotation line received from quarterion.
1865          * @param {enchant.gl.Quat} quat
1866          */
1867         rotationSet: function(quat) {
1868             quat.toMat4(this._rotation);
1869             this._changedRotation = true;
1870         },
1871 
1872         /**
1873          * Applies rotation line in rotation line received from quarterion.
1874          * @type {enchant.gl.Quat} quat
1875          */
1876         rotationApply: function(quat) {
1877             quat.toMat4(this.tmpMat);
1878             mat4.multiply(this._rotation, this.tmpMat);
1879             this._changedRotation = true;
1880         },
1881 
1882         /**
1883          * Rotate Sprite3D in local Z acxis.
1884          * @param {Number} radius
1885          */
1886         rotateRoll: function(rad) {
1887             this.rotationApply(new enchant.gl.Quat(0, 0, 1, rad));
1888             this._changedRotation = true;
1889         },
1890 
1891         /**
1892          * Rotate Sprite3D in local X acxis.
1893          * @param {Number} radius
1894          */
1895         rotatePitch: function(rad) {
1896             this.rotationApply(new enchant.gl.Quat(1, 0, 0, rad));
1897             this._changedRotation = true;
1898         },
1899 
1900         /**
1901          * Rotate Sprite3D in local Y acxis.
1902          * @param {Number} radius
1903          */
1904         rotateYaw: function(rad) {
1905             this.rotationApply(new enchant.gl.Quat(0, 1, 0, rad));
1906             this._changedRotation = true;
1907         },
1908 
1909         /**
1910          * @type {enchant.gl.Mesh}
1911          */
1912         mesh: {
1913             get: function() {
1914                 return this._mesh;
1915             },
1916             set: function(mesh) {
1917                 if (this.scene !== null) {
1918                     this._mesh._count -= 1;
1919                     mesh._count += 1;
1920                 }
1921                 this._mesh = mesh;
1922             }
1923         },
1924 
1925         /**
1926          * Conversion line applied to Sprite3D.
1927          * @deprecated
1928          * @type Number[]
1929          */
1930         matrix: {
1931             get: function() {
1932                 return this._matrix;
1933             },
1934             set: function(matrix) {
1935                 this._matrix = matrix;
1936             }
1937         },
1938 
1939         /**
1940          * Object used in Sprite3D collision detection.
1941          * @type enchant.gl.Bounding | enchant.gl.BS | enchant.gl.AABB
1942          */
1943         bounding: {
1944             get: function() {
1945                 return this._bounding;
1946             },
1947             set: function(bounding) {
1948                 this._bounding = bounding;
1949                 this._bounding.parent = this;
1950             }
1951         },
1952 
1953         /**
1954          * Determine whether to make Sprite3D touch compatible.
1955          * If set to false, will be ignored each time touch detection occurs.
1956          * @type bool
1957          */
1958         touchable: {
1959             get: function() {
1960                 return this._touchable;
1961             },
1962             set: function(bool) {
1963                 this._touchable = bool;
1964                 if (this._touchable) {
1965                     this.detectColor[3] = 1.0;
1966                 } else {
1967                     this.detectColor[3] = 0.0;
1968                 }
1969             }
1970         },
1971 
1972         _transform: function(baseMatrix) {
1973             if (this._changedTranslation ||
1974                 this._changedRotation ||
1975                 this._changedScale) {
1976                 mat4.identity(this.modelMat);
1977                 mat4.translate(this.modelMat, [this._x, this._y, this._z]);
1978                 mat4.multiply(this.modelMat, this._rotation, this.modelMat);
1979                 mat4.scale(this.modelMat, [this._scaleX, this._scaleY, this._scaleZ]);
1980                 mat4.multiply(this.modelMat, this._matrix, this.modelMat);
1981                 this._changedTranslation = false;
1982                 this._changedRotation = false;
1983                 this._changedScale = false;
1984             }
1985 
1986             mat4.multiply(baseMatrix, this.modelMat, this.tmpMat);
1987 
1988             this._global[0] = this._x;
1989             this._global[1] = this._y;
1990             this._global[2] = this._z;
1991             mat4.multiplyVec3(this.tmpMat, this._global);
1992             this.globalX = this._global[0];
1993             this.globalY = this._global[1];
1994             this.globalZ = this._global[2];
1995         },
1996 
1997         _render: function(detectTouch) {
1998             var useTexture = this.mesh.texture._image ? 1.0 : 0.0;
1999 
2000             mat4.toInverseMat3(this.tmpMat, this._normMat);
2001             mat3.transpose(this._normMat);
2002 
2003             var attributes = {
2004                 aVertexPosition: this.mesh._vertices,
2005                 aVertexColor: this.mesh._colors,
2006                 aNormal: this.mesh._normals,
2007                 aTextureCoord: this.mesh._texCoords
2008             };
2009 
2010             var uniforms = {
2011                 uModelMat: this.tmpMat,
2012                 uDetectColor: this.detectColor,
2013                 uSpecular: this.mesh.texture.specular,
2014                 uDiffuse: this.mesh.texture.diffuse,
2015                 uEmission: this.mesh.texture.emission,
2016                 uAmbient: this.mesh.texture.ambient,
2017                 uShininess: this.mesh.texture.shininess,
2018                 uNormMat: this._normMat,
2019                 uSampler: this.mesh.texture,
2020                 uUseTexture: useTexture
2021             };
2022 
2023             var length = this.mesh.indices.length;
2024             enchant.Core.instance.GL.renderElements(this.mesh._indices, 0, length, attributes, uniforms);
2025         },
2026 
2027         _draw: function(scene, detectTouch, baseMatrix) {
2028 
2029             this._transform(baseMatrix);
2030 
2031             if (this.childNodes.length) {
2032                 for (var i = 0, l = this.childNodes.length; i < l; i++) {
2033                     this.childNodes[i]._draw(scene, detectTouch, this.tmpMat);
2034                 }
2035             }
2036 
2037             this.dispatchEvent(new enchant.Event('prerender'));
2038 
2039             if (this.mesh !== null) {
2040                 if (this.program !== null) {
2041                     enchant.Core.instance.GL.setProgram(this.program);
2042                     this._render(detectTouch);
2043                     enchant.Core.instance.GL.setDefaultProgram();
2044                 } else {
2045                     this._render(detectTouch);
2046                 }
2047             }
2048 
2049             this.dispatchEvent(new enchant.Event('render'));
2050         }
2051     });
2052 
2053     /**
2054      * Sprite3D x coordinates.
2055      * @default 0
2056      * @type Number
2057      * @see enchant.gl.Sprite3D#translate
2058      */
2059     enchant.gl.Sprite3D.prototype.x = 0;
2060 
2061     /**
2062      * Sprite3D y coordinates.
2063      * @default 0
2064      * @type Number
2065      * @see enchant.gl.Sprite3D#translate
2066      */
2067     enchant.gl.Sprite3D.prototype.y = 0;
2068 
2069     /**
2070      * Sprite3D z coordinates.
2071      * @default 0
2072      * @type Number
2073      * @see enchant.gl.Sprite3D#translate
2074      */
2075     enchant.gl.Sprite3D.prototype.z = 0;
2076 
2077     'x y z'.split(' ').forEach(function(prop) {
2078         Object.defineProperty(enchant.gl.Sprite3D.prototype, prop, {
2079             get: function() {
2080                 return this['_' + prop];
2081             },
2082             set: function(n) {
2083                 this['_' + prop] = n;
2084                 this._changedTranslation = true;
2085             }
2086         });
2087     });
2088 
2089     /**
2090      * Sprite3D x axis direction expansion rate
2091      * @default 1.0
2092      * @type Number
2093      * @see enchant.gl.Sprite3D#scale
2094      */
2095     enchant.gl.Sprite3D.prototype.scaleX = 1;
2096 
2097     /**
2098      * Sprite3D y axis direction expansion rate
2099      * @default 1.0
2100      * @type Number
2101      * @see enchant.gl.Sprite3D#scale
2102      */
2103     enchant.gl.Sprite3D.prototype.scaleY = 1;
2104     /**
2105      * Sprite3D z axis direction expansion rate
2106      * @default 1.0
2107      * @type Number
2108      * @see enchant.gl.Sprite3D#scale
2109      */
2110     enchant.gl.Sprite3D.prototype.scaleZ = 1;
2111 
2112     'scaleX scaleY scaleZ'.split(' ').forEach(function(prop) {
2113         Object.defineProperty(enchant.gl.Sprite3D.prototype, prop, {
2114             get: function() {
2115                 return this['_' + prop];
2116             },
2117             set: function(scale) {
2118                 this['_' + prop] = scale;
2119                 this._changedScale = true;
2120             }
2121         });
2122     });
2123 
2124     /**
2125      * Sprite3D global x coordinates.
2126      * @default 0
2127      * @type Number
2128      * @see enchant.gl.Sprite3D#translate
2129      */
2130     enchant.gl.Sprite3D.prototype.globalX = 0;
2131 
2132     /**
2133      * Sprite 3D global y coordinates.
2134      * @default 0
2135      * @type Number
2136      * @see enchant.gl.Sprite3D#translate
2137      */
2138     enchant.gl.Sprite3D.prototype.globalY = 0;
2139 
2140     /**
2141      * Sprite3D global 3D coordinates.
2142      * @default 0
2143      * @type Number
2144      * @see enchant.gl.Sprite3D#translate
2145      */
2146     enchant.gl.Sprite3D.prototype.globalZ = 0;
2147 
2148     /**
2149      * @scope enchant.gl.Camera3D.prototype
2150      */
2151     enchant.gl.Camera3D = enchant.Class.create({
2152         /**
2153          * Class to set 3D scene camera
2154          * @example
2155          * var scene = new Scene3D();
2156          * var camera = new Camera3D();
2157          * camera.x = 0;
2158          * camera.y = 0;
2159          * camera.z = 10;
2160          * scene.setCamera(camera);
2161          * @constructs
2162          */
2163         initialize: function() {
2164             var core = enchant.Core.instance;
2165             this.mat = mat4.identity();
2166             this.invMat = mat4.identity();
2167             this.invMatY = mat4.identity();
2168             this._projMat = mat4.create();
2169             mat4.perspective(20, core.width / core.height, 1.0, 1000.0, this._projMat);
2170             this._changedPosition = false;
2171             this._changedCenter = false;
2172             this._changedUpVector = false;
2173             this._changedProjection = false;
2174             this._x = 0;
2175             this._y = 0;
2176             this._z = 10;
2177             this._centerX = 0;
2178             this._centerY = 0;
2179             this._centerZ = 0;
2180             this._upVectorX = 0;
2181             this._upVectorY = 1;
2182             this._upVectorZ = 0;
2183         },
2184         /**
2185          * projection matrix
2186          */
2187         projMat: {
2188             get: function() {
2189                 return this._projMat;
2190             },
2191             set: function(mat) {
2192                 this._projMat = mat;
2193                 this._changedProjection = true;
2194             }
2195         },
2196         /**
2197          * Fit camera perspective to Sprite3D position.
2198          * @param {enchant.gl.Sprite3D} sprite Sprite3D being focused on
2199          */
2200         lookAt: function(sprite) {
2201             if (sprite instanceof enchant.gl.Sprite3D) {
2202                 this._centerX = sprite.x;
2203                 this._centerY = sprite.y;
2204                 this._centerZ = sprite.z;
2205                 this._changedCenter = true;
2206             }
2207         },
2208         /**
2209          * Bring camera position closer to that of Sprite3D.
2210          * @param {enchant.gl.Sprite3D} sprite Target Sprite3D
2211          * @param {Number} position Distance from target
2212          * @param {Number} speed Speed of approach to target
2213          * @example
2214          * var sp = new Sprite3D();
2215          * var camera = new Camera3D();
2216          * // Continue approaching camera position from 10 behind while focusing on sp
2217          * sp.addEventListener('enterframe', function() {
2218          *     camera.lookAt(sp);
2219          *     camera.chase(sp, -10, 20);
2220          * });
2221          */
2222         chase: function(sprite, position, speed) {
2223             if (sprite instanceof enchant.gl.Sprite3D) {
2224                 var vx = sprite.x + sprite.rotation[8] * position;
2225                 var vy = sprite.y + sprite.rotation[9] * position;
2226                 var vz = sprite.z + sprite.rotation[10] * position;
2227                 this._x += (vx - this._x) / speed;
2228                 this._y += (vy - this._y) / speed;
2229                 this._z += (vz - this._z) / speed;
2230                 this._changedPosition = true;
2231             }
2232         },
2233         _getForwardVec: function() {
2234             var x = this._centerX - this._x;
2235             var y = this._centerY - this._y;
2236             var z = this._centerZ - this._z;
2237             return vec3.normalize([x, y, z]);
2238         },
2239         _getSideVec: function() {
2240             var f = this._getForwardVec();
2241             var u = this._getUpVec();
2242             return vec3.cross(u, f);
2243         },
2244         _getUpVec: function() {
2245             var x = this._upVectorX;
2246             var y = this._upVectorY;
2247             var z = this._upVectorZ;
2248             return [x, y, z];
2249         },
2250         _move: function(v, s) {
2251             v[0] *= s;
2252             v[1] *= s;
2253             v[2] *= s;
2254             this._x += v[0];
2255             this._y += v[1];
2256             this._z += v[2];
2257             this._centerX += v[0];
2258             this._centerY += v[1];
2259             this._centerZ += v[2];
2260         },
2261         /**
2262          * Moves forward Camera3D.
2263          * @param {Number} speed
2264          */
2265         forward: function(s) {
2266             var v = this._getForwardVec();
2267             this._move(v, s);
2268         },
2269         /**
2270          * Moves side Camera3D.
2271          * @param {Number} speed
2272          */
2273         sidestep: function(s) {
2274             var v = this._getSideVec();
2275             this._move(v, s);
2276         },
2277         /**
2278          * Moves up Camera3D.
2279          * @param {Number} speed
2280          */
2281         altitude: function(s) {
2282             var v = this._getUpVec();
2283             this._move(v, s);
2284         },
2285         /**
2286          * Rotate Camera3D in local Z acxis.
2287          * @param {Number} radius
2288          */
2289         rotateRoll: function(rad) {
2290             var u = this._getUpVec();
2291             var f = this._getForwardVec();
2292             var x = f[0];
2293             var y = f[1];
2294             var z = f[2];
2295             var quat = new enchant.gl.Quat(x, y, z, -rad);
2296             var vec = quat.multiplyVec3(u);
2297             this._upVectorX = vec[0];
2298             this._upVectorY = vec[1];
2299             this._upVectorZ = vec[2];
2300             this._changedUpVector = true;
2301         },
2302         /**
2303          * Rotate Camera3D in local X acxis.
2304          * @param {Number} radius
2305          */
2306         rotatePitch: function(rad) {
2307             var u = this._getUpVec();
2308             var f = this._getForwardVec();
2309             var s = this._getSideVec();
2310             var sx = s[0];
2311             var sy = s[1];
2312             var sz = s[2];
2313             var quat = new enchant.gl.Quat(sx, sy, sz, -rad);
2314             var vec = quat.multiplyVec3(f);
2315             this._centerX = this._x + vec[0];
2316             this._centerY = this._y + vec[1];
2317             this._centerZ = this._z + vec[2];
2318             vec = vec3.normalize(quat.multiplyVec3(u));
2319             this._upVectorX = vec[0];
2320             this._upVectorY = vec[1];
2321             this._upVectorZ = vec[2];
2322             this._changedCenter = true;
2323             this._changedUpVector = true;
2324         },
2325         /**
2326          * Rotate Camera3D in local Y acxis.
2327          * @param {Number} radius
2328          */
2329         rotateYaw: function(rad) {
2330             var u = this._getUpVec();
2331             var ux = u[0];
2332             var uy = u[1];
2333             var uz = u[2];
2334             var f = this._getForwardVec();
2335             var quat = new enchant.gl.Quat(ux, uy, uz, -rad);
2336             var vec = quat.multiplyVec3(f);
2337             this._centerX = this._x + vec[0];
2338             this._centerY = this._y + vec[1];
2339             this._centerZ = this._z + vec[2];
2340             this._changedCenter = true;
2341         },
2342         _updateMatrix: function() {
2343             mat4.lookAt(
2344                 [this._x, this._y, this._z],
2345                 [this._centerX, this._centerY, this._centerZ],
2346                 [this._upVectorX, this._upVectorY, this._upVectorZ],
2347                 this.mat);
2348             mat4.lookAt(
2349                 [0, 0, 0],
2350                 [-this._x + this._centerX,
2351                     -this._y + this._centerY,
2352                     -this._z + this._centerZ],
2353                 [this._upVectorX, this._upVectorY, this._upVectorZ],
2354                 this.invMat);
2355             mat4.inverse(this.invMat);
2356             mat4.lookAt(
2357                 [0, 0, 0],
2358                 [-this._x + this._centerX,
2359                     0,
2360                     -this._z + this._centerZ],
2361                 [this._upVectorX, this._upVectorY, this._upVectorZ],
2362                 this.invMatY);
2363             mat4.inverse(this.invMatY);
2364         }
2365     });
2366 
2367     /**
2368      * Camera x coordinates
2369      * @type Number
2370      */
2371     enchant.gl.Camera3D.prototype.x = 0;
2372 
2373     /**
2374      * Camera y coordinates
2375      * @type Number
2376      */
2377     enchant.gl.Camera3D.prototype.y = 0;
2378 
2379     /**
2380      * Camera z coordinates
2381      * @type Number
2382      */
2383     enchant.gl.Camera3D.prototype.z = 0;
2384 
2385     'x y z'.split(' ').forEach(function(prop) {
2386         Object.defineProperty(enchant.gl.Camera3D.prototype, prop, {
2387             get: function() {
2388                 return this['_' + prop];
2389             },
2390             set: function(n) {
2391                 this['_' + prop] = n;
2392                 this._changedPosition = true;
2393             }
2394         });
2395     });
2396 
2397     /**
2398      * Camera perspective x coordinates
2399      * @type Number
2400      */
2401     enchant.gl.Camera3D.prototype.centerX = 0;
2402 
2403     /**
2404      * Camera perspective y coordinates
2405      * @type Number
2406      */
2407     enchant.gl.Camera3D.prototype.centerY = 0;
2408 
2409     /**
2410      * Camera perspective z coordinates
2411      * @type Number
2412      */
2413     enchant.gl.Camera3D.prototype.centerZ = 0;
2414 
2415     'centerX centerY centerZ'.split(' ').forEach(function(prop) {
2416         Object.defineProperty(enchant.gl.Camera3D.prototype, prop, {
2417             get: function() {
2418                 return this['_' + prop];
2419             },
2420             set: function(n) {
2421                 this['_' + prop] = n;
2422                 this._changedCenter = true;
2423             }
2424         });
2425     });
2426 
2427     /**
2428      * Camera upper vector x component
2429      * @type Number
2430      */
2431     enchant.gl.Camera3D.prototype.upVectorX = 0;
2432 
2433     /**
2434      * Camera upper vector y component
2435      * @type Number
2436      */
2437     enchant.gl.Camera3D.prototype.upVectorY = 1;
2438 
2439     /**
2440      * Camera upper vector z component
2441      * @type Number
2442      */
2443     enchant.gl.Camera3D.prototype.upVectorZ = 0;
2444 
2445     'upVectorX upVectorY upVectorZ'.split(' ').forEach(function(prop) {
2446         Object.defineProperty(enchant.gl.Camera3D.prototype, prop, {
2447             get: function() {
2448                 return this['_' + prop];
2449             },
2450             set: function(n) {
2451                 this['_' + prop] = n;
2452                 this._changedUpVector = true;
2453             }
2454         });
2455     });
2456 
2457     /**
2458      * @scope enchant.gl.Scene3D.prototype
2459      */
2460     enchant.gl.Scene3D = enchant.Class.create(enchant.EventTarget, {
2461         /**
2462          * Class for displayed Sprite3D tree route.
2463          * Currently, multiple definitions are impossible, and the Scene3D defined first will be returned.
2464          *
2465          * @example
2466          *   var scene = new Scene3D();
2467          *   var sprite = new Sprite3D();
2468          *   scene.addChild(sprite);
2469          *
2470          * @constructs
2471          * @extends enchant.EventTarget
2472          */
2473         initialize: function() {
2474             var core = enchant.Core.instance;
2475             if (core.currentScene3D) {
2476                 return core.currentScene3D;
2477             }
2478             enchant.EventTarget.call(this);
2479             /**
2480              * Child element array.
2481              * A list of Sprite3D added as child classes in this scene can be acquired.
2482              * When adding or subtracting child classes, without directly operating this array,
2483              * {@link enchant.gl.Scene3D#addChild} or {@link enchant.gl.Scene3D#removeChild} is used.
2484              * @type enchant.gl.Sprite3D[]
2485              */
2486             this.childNodes = [];
2487 
2488             /**
2489              * Lighting array.
2490              * At present, the only light source active in the scene is 0.
2491              * Acquires a list of light sources acquired in this scene.
2492              * When lighting is added or deleted, without directly operating this array,
2493              * {@link enchant.gl.Scene3D#addLight} or {@link enchant.gl.Scene3D#removeLight} is used.
2494              * @type enchant.gl.PointLight[]
2495              */
2496             this.lights = [];
2497 
2498             this.identityMat = mat4.identity();
2499             this._backgroundColor = [0.0, 0.0, 0.0, 1.0];
2500 
2501             var listener = function(e) {
2502                 for (var i = 0, len = this.childNodes.length; i < len; i++) {
2503                     var sprite = this.childNodes[i];
2504                     sprite.dispatchEvent(e);
2505                 }
2506             };
2507             this.addEventListener('added', listener);
2508             this.addEventListener('removed', listener);
2509             this.addEventListener('addedtoscene', listener);
2510             this.addEventListener('removedfromscene', listener);
2511 
2512             var that = this;
2513             var func = function() {
2514                 that._draw();
2515             };
2516             core.addEventListener('enterframe', func);
2517 
2518 
2519             var uniforms = {};
2520             uniforms['uUseCamera'] = 0.0;
2521             gl.activeTexture(gl.TEXTURE0);
2522             core.GL.defaultProgram.setUniforms(uniforms);
2523 
2524             if (core.currentScene3D === null) {
2525                 core.currentScene3D = this;
2526             }
2527 
2528             this.setAmbientLight(new enchant.gl.AmbientLight());
2529             this.setDirectionalLight(new enchant.gl.DirectionalLight());
2530             this.setCamera(new enchant.gl.Camera3D());
2531         },
2532 
2533         /**
2534          * Scene3D background color
2535          * @type Number[]
2536          */
2537         backgroundColor: {
2538             get: function() {
2539                 return this._backgroundColor;
2540             },
2541             set: function(arg) {
2542                 var c = enchant.Core.instance.GL.parseColor(arg);
2543                 this._backgroundColor = c;
2544                 gl.clearColor(c[0], c[1], c[2], c[3]);
2545 
2546             }
2547         },
2548 
2549         /**
2550          * Add Sprite3D to scene.
2551          * Adds Sprite3D delivered to argument and child classes to scene.
2552          * Sprite3D will automatically be displayed on screen if added to scene.
2553          * Use {@link enchant.gl.Scene3D#removeChild} to delete object added once.
2554          * @param {enchant.gl.Sprite3D} sprite Sprite3D to be added
2555          * @see enchant.gl.Scene3D#removeChild
2556          * @see enchant.gl.Scene3D#childNodes
2557          */
2558         addChild: function(sprite) {
2559             this.childNodes.push(sprite);
2560             sprite.parentNode = sprite.scene = this;
2561             sprite.dispatchEvent(new enchant.Event('added'));
2562             sprite.dispatchEvent(new enchant.Event('addedtoscene'));
2563             sprite.dispatchEvent(new enchant.Event('render'));
2564         },
2565 
2566         /**
2567          * Delete Sprite3D from scene.
2568          * Deletes designated Sprite3D from scene.
2569          * The deleted Sprite3D will no longer be displayed on the screen.
2570          * Use {@link enchant.gl.Scene3D#addChild} to add Sprite3D.
2571          * @param {enchant.gl.Sprite3D} sprite Sprite3D to delete
2572          * @see enchant.gl.Scene3D#addChild
2573          * @see enchant.gl.Scene3D#childNodes
2574          */
2575         removeChild: function(sprite) {
2576             var i;
2577             if ((i = this.childNodes.indexOf(sprite)) !== -1) {
2578                 this.childNodes.splice(i, 1);
2579                 sprite.parentNode = sprite.scene = null;
2580                 sprite.dispatchEvent(new enchant.Event('removed'));
2581                 sprite.dispatchEvent(new enchant.Event('removedfromscene'));
2582             }
2583         },
2584 
2585         /**
2586          * Sets scene's camera postion.
2587          * @param {enchant.gl.Camera3D} camera Camera to set
2588          * @see enchant.gl.Camera3D
2589          */
2590         setCamera: function(camera) {
2591             camera._changedPosition = true;
2592             camera._changedCenter = true;
2593             camera._changedUpVector = true;
2594             camera._changedProjection = true;
2595             this._camera = camera;
2596             enchant.Core.instance.GL.defaultProgram.setUniforms({
2597                 uUseCamera: 1.0
2598             });
2599         },
2600 
2601         /**
2602          * Gets camera source in scene.
2603          * @see enchant.gl.Camera3D
2604          * @return {enchant.gl.Camera}
2605          */
2606         getCamera: function() {
2607             return this._camera;
2608         },
2609 
2610         /**
2611          * Sets ambient light source in scene.
2612          * @param {enchant.gl.AmbientLight} light Lighting to set
2613          * @see enchant.gl.AmbientLight
2614          */
2615         setAmbientLight: function(light) {
2616             this.ambientLight = light;
2617         },
2618 
2619         /**
2620          * Gets ambient light source in scene.
2621          * @see enchant.gl.AmbientLight
2622          * @return {enchant.gl.AmbientLight}
2623          */
2624         getAmbientLight: function() {
2625             return this.ambientLight;
2626         },
2627 
2628         /**
2629          * Sets directional light source in scene.
2630          * @param {enchant.gl.DirectionalLight} light Lighting to set
2631          * @see enchant.gl.DirectionalLight
2632          */
2633         setDirectionalLight: function(light) {
2634             this.directionalLight = light;
2635             this.useDirectionalLight = true;
2636             enchant.Core.instance.GL.defaultProgram.setUniforms({
2637                 uUseDirectionalLight: 1.0
2638             });
2639         },
2640 
2641         /**
2642          * Gets directional light source in scene.
2643          * @see enchant.gl.DirectionalLight
2644          * @return {enchant.gl.DirectionalLight}
2645          */
2646         getDirectionalLight: function() {
2647             return this.directionalLight;
2648         },
2649 
2650         /**
2651          * Add lighting to scene.
2652          * Currently, will not be used even if added to scene.
2653          * @param {enchant.gl.PointLight} light Lighting to add
2654          * @see enchant.gl.PointLight
2655          */
2656         addLight: function(light) {
2657             this.lights.push(light);
2658             this.usePointLight = true;
2659         },
2660 
2661         /**
2662          * Delete lighting from scene
2663          * @param {enchant.gl.PointLight} light Lighting to delete
2664          * @see enchant.gl.PointLight.
2665          */
2666         removeLight: function(light) {
2667             var i;
2668             if ((i = this.lights.indexOf(light)) !== -1) {
2669                 this.lights.splice(i, 1);
2670             }
2671         },
2672 
2673         _draw: function(detectTouch) {
2674             var core = enchant.Core.instance;
2675             var program = core.GL.defaultProgram;
2676 
2677             gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
2678 
2679             var detect = (detectTouch === 'detect') ? 1.0 : 0.0;
2680 
2681             var uniforms = { uDetectTouch: detect };
2682 
2683             if (this.ambientLight._changedColor) {
2684                 uniforms['uAmbientLightColor'] = this.ambientLight.color;
2685                 this.ambientLight._changedColor = false;
2686             }
2687             if (this.useDirectionalLight) {
2688                 if (this.directionalLight._changedDirection) {
2689                     uniforms['uLightDirection'] = [
2690                         this.directionalLight.directionX,
2691                         this.directionalLight.directionY,
2692                         this.directionalLight.directionZ
2693                     ];
2694                     this.directionalLight._changedDirection = false;
2695                 }
2696                 if (this.directionalLight._changedColor) {
2697                     uniforms['uLightColor'] = this.directionalLight.color;
2698                     this.directionalLight._changedColor = false;
2699                 }
2700             }
2701 
2702             if (this._camera) {
2703                 if (this._camera._changedPosition ||
2704                     this._camera._changedCenter ||
2705                     this._camera._changedUpVector ||
2706                     this._camera._changedProjection) {
2707                     this._camera._updateMatrix();
2708                     uniforms['uCameraMat'] = this._camera.mat;
2709                     uniforms['uProjMat'] = this._camera._projMat;
2710                     uniforms['uLookVec'] = [
2711                         this._camera._centerX - this._camera._x,
2712                         this._camera._centerY - this._camera._y,
2713                         this._camera._centerZ - this._camera._z
2714                     ];
2715                     this._camera._changedPosition = false;
2716                     this._camera._changedCenter = false;
2717                     this._camera._changedUpVector = false;
2718                     this._camera._changedProjection = false;
2719                 }
2720             }
2721             program.setUniforms(uniforms);
2722 
2723             mat4.identity(this.identityMat);
2724             for (var i = 0, l = this.childNodes.length; i < l; i++) {
2725                 this.childNodes[i]._draw(this, detectTouch, this.identityMat);
2726             }
2727 
2728         }
2729     });
2730 
2731     /**
2732      * @type {Object}
2733      */
2734     enchant.gl.collision = {};
2735 
2736     var point2point = function(p1, p2) {
2737         var vx = p1.x + p1.parent.x - p2.x - p2.parent.x;
2738         var vy = p1.y + p1.parent.y - p2.y - p2.parent.y;
2739         var vz = p1.z + p1.parent.z - p2.z - p2.parent.z;
2740         return (vx * vx + vy * vy + vz * vz);
2741     };
2742 
2743     var point2BS = function(p, bs) {
2744         return (point2point(p, bs) - bs.radius * bs.radius);
2745     };
2746 
2747     var point2AABB = function(p, aabb) {
2748         var ppx = p.x + p.parent.x;
2749         var ppy = p.y + p.parent.y;
2750         var ppz = p.z + p.parent.z;
2751         var px = aabb.parent.x + aabb.x + aabb.scale;
2752         var py = aabb.parent.y + aabb.y + aabb.scale;
2753         var pz = aabb.parent.z + aabb.z + aabb.scale;
2754         var nx = aabb.parent.x + (aabb.x - aabb.scale);
2755         var ny = aabb.parent.y + (aabb.y - aabb.scale);
2756         var nz = aabb.parent.z + (aabb.z - aabb.scale);
2757         var dist = 0;
2758         if (ppx < nx) {
2759             dist += (ppx - nx) * (ppx - nx);
2760         } else if (px < ppx) {
2761             dist += (ppx - px) * (ppx - px);
2762         }
2763         if (ppy < ny) {
2764             dist += (ppy - ny) * (ppy - ny);
2765         } else if (py < ppy) {
2766             dist += (ppy - py) * (ppy - py);
2767         }
2768         if (ppz < nz) {
2769             dist += (ppz - nz) * (ppz - nz);
2770         } else if (pz < ppz) {
2771             dist += (ppz - pz) * (ppz - pz);
2772         }
2773         return dist;
2774     };
2775 
2776     var point2OBB = function(p, obb) {
2777         return 1;
2778     };
2779 
2780     var BS2BS = function(bs1, bs2) {
2781         return (point2point(bs1, bs2) - (bs1.radius + bs2.radius) * (bs1.radius + bs2.radius));
2782     };
2783 
2784     var BS2AABB = function(bs, aabb) {
2785         return (point2AABB(bs, aabb) - bs.radius * bs.radius);
2786     };
2787 
2788     var BS2OBB = function(bs, obb) {
2789         return 1;
2790     };
2791 
2792     var AABB2AABB = function(aabb1, aabb2) {
2793         var px1 = aabb1.parent.x + aabb1.x + aabb1.scale;
2794         var py1 = aabb1.parent.y + aabb1.y + aabb1.scale;
2795         var pz1 = aabb1.parent.z + aabb1.z + aabb1.scale;
2796 
2797         var nx1 = aabb1.parent.x + (aabb1.x - aabb1.scale);
2798         var ny1 = aabb1.parent.y + (aabb1.y - aabb1.scale);
2799         var nz1 = aabb1.parent.z + (aabb1.z - aabb1.scale);
2800 
2801         var px2 = aabb2.parent.x + aabb2.x + aabb2.scale;
2802         var py2 = aabb2.parent.y + aabb2.y + aabb2.scale;
2803         var pz2 = aabb2.parent.z + aabb2.z + aabb2.scale;
2804 
2805         var nx2 = aabb2.parent.x + (aabb2.x - aabb2.scale);
2806         var ny2 = aabb2.parent.y + (aabb2.y - aabb2.scale);
2807         var nz2 = aabb2.parent.z + (aabb2.z - aabb2.scale);
2808         return ((nx2 <= px1) && (nx1 <= px2) &&
2809             (ny2 <= py1) && (ny1 <= py2) &&
2810             (nz2 <= pz1) && (nz1 <= pz2)) ? 0.0 : 1.0;
2811     };
2812 
2813     var AABB2OBB = function(aabb, obb) {
2814         return 1;
2815     };
2816 
2817     var OBB2OBB = function(obb1, obb2) {
2818         return 1;
2819     };
2820 
2821     /**
2822      * @scope enchant.gl.collision.Bounding.prototype
2823      */
2824     enchant.gl.collision.Bounding = enchant.Class.create({
2825         /**
2826          * Class to set Sprite3D collision detection.
2827          * Defined as a point.
2828          * {@link enchant.gl.collision.BS}, {@link enchant.gl.collision.AABB},
2829          * {@link enchant.gl.collision.OBB} exist as
2830          * inherited classes of enchant.gl.collision.Bounding
2831          * Currently, OBB is not supported.
2832          * @constructs
2833          */
2834         initialize: function() {
2835             this.type = 'point';
2836             this.threshold = 0.0001;
2837             this.x = 0;
2838             this.y = 0;
2839             this.z = 0;
2840             this.parent = {
2841                 x: 0,
2842                 y: 0,
2843                 z: 0
2844             };
2845         },
2846         /**
2847          * Calculates distance between points.
2848          * @param {enchant.gl.collision.Bounding} bounding Collision point object
2849          * @return {Number}
2850          */
2851         toBounding: function(another) {
2852             return point2point(this, another);
2853         },
2854         /**
2855          * Calculates distance between balls.
2856          * @param {enchant.gl.collision.BS} boudning Collision ball object
2857          * @return {Number}
2858          */
2859         toBS: function(another) {
2860             return point2BS(this, another);
2861         },
2862         /**
2863          * Calculates distance from non-rotating cube.
2864          * Currently, 0 will be returned with or without collision.
2865          * @param {enchant.gl.collision.AABB} bounding AABB
2866          * @return {Number}
2867          */
2868         toAABB: function(another) {
2869             return point2AABB(this, another);
2870         },
2871         /**
2872          * Calculates distance from rotating cuboid.
2873          * Not currently supported.
2874          * @param {enchant.gl.collision.OBB} bounding OBB
2875          * @return {Number}
2876          */
2877         toOBB: function(another) {
2878             return point2OBB(this, another);
2879         },
2880         /**
2881          * Collision detection with other collision detection object.
2882          * A collision detection object can detect collision with an object with x, y, z properties.
2883          * @param {enchant.gl.collision.Bounding|enchant.gl.collision.BS|enchant.gl.collision.AABB|enchant.gl.collision.OBB} bounding Collision detection object
2884          * @return {Boolean}
2885          */
2886         intersect: function(another) {
2887             switch (another.type) {
2888                 case 'point':
2889                     return (this.toBounding(another) < this.threshold);
2890                 case 'BS':
2891                     return (this.toBS(another) < this.threshold);
2892                 case 'AABB':
2893                     return (this.toAABB(another) < this.threshold);
2894                 case 'OBB':
2895                     return (this.toOBB(another) < this.threshold);
2896                 default:
2897                     return false;
2898             }
2899         }
2900     });
2901 
2902     /**
2903      * @scope enchant.gl.collision.BS.prototype
2904      */
2905     enchant.gl.collision.BS = enchant.Class.create(enchant.gl.collision.Bounding, {
2906         /**
2907          * Class that sets Sprite3D collision detection.
2908          * Defined as a ball.
2909          * @constructs
2910          * @see enchant.gl.collision.Bounding
2911          */
2912         initialize: function() {
2913             enchant.gl.collision.Bounding.call(this);
2914             this.type = 'BS';
2915             this.radius = 0.5;
2916         },
2917         toBounding: function(another) {
2918             return point2BS(another, this);
2919         },
2920         toBS: function(another) {
2921             return BS2BS(this, another);
2922         },
2923         toAABB: function(another) {
2924             return BS2AABB(this, another);
2925         },
2926         toOBB: function(another) {
2927             return BS2OBB(this, another);
2928         }
2929     });
2930 
2931     /**
2932      * @scope enchant.gl.collision.AABB.prototype
2933      */
2934     enchant.gl.collision.AABB = enchant.Class.create(enchant.gl.collision.Bounding, {
2935         /**
2936          * Class that sets Sprite3D collision detection.
2937          * Defined as non-rotating cube.
2938          * @constructs
2939          * @see enchant.gl.collision.Bounding
2940          */
2941         initialize: function() {
2942             enchant.gl.collision.Bounding.call(this);
2943             this.type = 'AABB';
2944             this.scale = 0.5;
2945         },
2946         toBounding: function(another) {
2947             return point2AABB(another, this);
2948         },
2949         toBS: function(another) {
2950             return BS2AABB(another, this);
2951         },
2952         toAABB: function(another) {
2953             return AABB2AABB(this, another);
2954         },
2955         toOBB: function(another) {
2956             return AABB2OBB(this, another);
2957         }
2958     });
2959 
2960     /**
2961      * @scope enchant.gl.collision.OBB.prototype
2962      */
2963     enchant.gl.collision.OBB = enchant.Class.create(enchant.gl.collision.Bounding, {
2964         /**
2965          * Class that sets Sprite3D collision detection.
2966          * Defined as rotating.
2967          * @constructs
2968          * @see enchant.gl.collision.Bounding
2969 
2970          */
2971         initialize: function() {
2972             enchant.gl.collision.Bounding.call(this);
2973             this.type = 'OBB';
2974         },
2975         toBounding: function(another) {
2976             return point2OBB(another, this);
2977         },
2978         toBS: function(another) {
2979             return BS2OBB(another, this);
2980         },
2981         toAABB: function(another) {
2982             return AABB2OBB(another, this);
2983         },
2984         toOBB: function(another) {
2985             return OBB2OBB(this, another);
2986         }
2987     });
2988 
2989     // borrowed from MMD.js
2990     var bezierp = function(x1, x2, y1, y2, x) {
2991         var t, tt, v;
2992         t = x;
2993         while (true) {
2994             v = ipfunc(t, x1, x2) - x;
2995             if (v * v < 0.0000001) {
2996                 break;
2997             }
2998             tt = ipfuncd(t, x1, x2);
2999             if (tt === 0) {
3000                 break;
3001             }
3002             t -= v / tt;
3003         }
3004         return ipfunc(t, y1, y2);
3005     };
3006     var ipfunc = function(t, p1, p2) {
3007         return (1 + 3 * p1 - 3 * p2) * t * t * t + (3 * p2 - 6 * p1) * t * t + 3 * p1 * t;
3008     };
3009     var ipfuncd = function(t, p1, p2) {
3010         return (3 + 9 * p1 - 9 * p2) * t * t + (6 * p2 - 12 * p1) * t + 3 * p1;
3011     };
3012     var frac = function(n1, n2, t) {
3013         return (t - n1) / (n2 - n1);
3014     };
3015     var lerp = function(n1, n2, r) {
3016         return n1 + r * (n2 - n1);
3017     };
3018 
3019     var _tmpve = vec3.create();
3020     var _tmpvt = vec3.create();
3021     var _tmpaxis = vec3.create();
3022     var _tmpquat = quat4.create();
3023     var _tmpinv = quat4.create();
3024 
3025     /**
3026      * @scope enchant.gl.State.prototype
3027      */
3028     enchant.gl.State = enchant.Class.create({
3029         /**
3030          * Base class for expressing animation condition.
3031          * @param {Number[]} position
3032          * @param {Number[]} rotation
3033          * @constructs
3034          */
3035         initialize: function(position, rotation) {
3036             this._position = vec3.create();
3037             vec3.set(position, this._position);
3038             this._rotation = quat4.create();
3039             quat4.set(rotation, this._rotation);
3040         },
3041         /**
3042          * Sets position/rotation.
3043          */
3044         set: function(pose) {
3045             vec3.set(pose._position, this._position);
3046             quat4.set(pose._rotation, this._rotation);
3047         }
3048     });
3049 
3050     /**
3051      * @scope enchant.gl.Pose.prototype
3052      */
3053     enchant.gl.Pose = enchant.Class.create(enchant.gl.State, {
3054         /**
3055          * Class for processing pose.
3056          * @param {Number[]} position
3057          * @param {Number[]} rotation
3058          * @constructs
3059          * @extends enchant.gl.State
3060          */
3061         initialize: function(position, rotation) {
3062             enchant.gl.State.call(this, position, rotation);
3063         },
3064         /**
3065          * Performs interpolation with other pose.
3066          * @param {enchant.gl.Pose} another
3067          * @param {Number} ratio
3068          * @return {enchant.gl.Pose}
3069          */
3070         getInterpolation: function(another, ratio) {
3071             vec3.lerp(this._position, another._position, ratio, _tmpve);
3072             quat4.slerp(this._rotation, another._rotation, ratio, _tmpquat);
3073             return new enchant.gl.Pose(_tmpve, _tmpquat);
3074         },
3075         _bezierp: function(x1, y1, x2, y2, x) {
3076             return bezierp(x1, x2, y1, y2, x);
3077         }
3078     });
3079 
3080     /**
3081      * @scope enchant.gl.KeyFrameManager.prototype
3082      */
3083     enchant.gl.KeyFrameManager = enchant.Class.create({
3084         /**
3085          * Class for realizing key frame animation.
3086          * Handles various data, not limited to enchant.gl.Pose.
3087          * @constructs
3088          */
3089         initialize: function() {
3090             this._frames = [];
3091             this._units = [];
3092             this.length = -1;
3093             this._lastPose = null;
3094         },
3095         /**
3096          * Add frame.
3097          * @param {*} pose Key frame.
3098          * @param {Number} frame Frame number.
3099          */
3100         addFrame: function(pose, frame) {
3101             if (typeof frame !== 'number') {
3102                 this.length += 1;
3103                 frame = this.length;
3104             }
3105             if (frame > this.length) {
3106                 this.length = frame;
3107                 this._lastPose = pose;
3108             }
3109             this._frames.push(frame);
3110             this._units[frame] = pose;
3111         },
3112         /**
3113          * Return information for designated frame number.
3114          * When there is no data corresponding to the designated frame, interpolated data from before and after are acquired.
3115          * @param {Number} frame Frame number
3116          * @return {*}
3117          */
3118         getFrame: function(frame) {
3119             var prev, next, index, pidx, nidx;
3120             var ratio = 0;
3121             if (frame >= this.length) {
3122                 return this._lastPose;
3123             }
3124             if (this._units[frame]) {
3125                 return this._units[frame];
3126             } else {
3127                 index = this._getPrevFrameIndex(frame);
3128                 pidx = this._frames[index];
3129                 nidx = this._frames[index + 1];
3130                 prev = this._units[pidx];
3131                 next = this._units[nidx];
3132                 ratio = this._frac(pidx, nidx, frame);
3133                 return this._interpole(prev, next, ratio);
3134             }
3135         },
3136         bake: function() {
3137             var state;
3138             for (var i = 0, l = this.length; i < l; i++) {
3139                 if (this._units[i]) {
3140                     continue;
3141                 }
3142                 state = this.getFrame(i);
3143                 this.addFrame(state, i);
3144             }
3145             this._sort();
3146         },
3147         _frac: function(p, n, t) {
3148             return frac(p, n, t);
3149         },
3150         _interpole: function(prev, next, ratio) {
3151             return prev.getInterpolation(next, ratio);
3152         },
3153         _sort: function() {
3154             this._frames.sort(function(a, b) {
3155                 return a - b;
3156             });
3157         },
3158         _getPrevFrameIndex: function(frame) {
3159             for (var i = 0, l = this._frames.length; i < l; i++) {
3160                 if (this._frames[i] > frame) {
3161                     break;
3162                 }
3163             }
3164             return i - 1;
3165         }
3166     });
3167 
3168     /**
3169      * @scope enchant.gl.Bone.prototype
3170      */
3171     enchant.gl.Bone = enchant.Class.create(enchant.gl.State, {
3172         /**
3173          * Class to display bone status.
3174          * @param {String} name
3175          * @param {Number} head
3176          * @param {Number} position
3177          * @param {Number} rotation
3178          * @constructs
3179          * @extends enchant.gl.State
3180          */
3181         initialize: function(name, head, position, rotation) {
3182             enchant.gl.State.call(this, position, rotation);
3183             this._name = name;
3184             this._origin = vec3.create();
3185 
3186             vec3.set(head, this._origin);
3187 
3188             this._globalpos = vec3.create();
3189             vec3.set(head, this._globalpos);
3190 
3191             this._globalrot = quat4.identity();
3192 
3193             this.parentNode = null;
3194             this.childNodes = [];
3195 
3196             /**
3197              * During each IK settlement, function for which change is applied to quaternion is set.
3198              */
3199             this.constraint = null;
3200         },
3201         /**
3202          * Add child bone to bone.
3203          * @param {enchant.gl.Bone} child
3204          */
3205         addChild: function(child) {
3206             this.childNodes.push(child);
3207             child.parentNode = this;
3208         },
3209         /**
3210          * Delete child bone from bone.
3211          * @param {enchant.gl.Bone} child
3212          */
3213         removeChild: function(child) {
3214             var i;
3215             if ((i = this.childNodes.indexOf(child)) !== -1) {
3216                 this.childNodes.splice(i, 1);
3217             }
3218             child.parentNode = null;
3219         },
3220         /**
3221          * Set bone pose.
3222          * @param {*} poses
3223          */
3224         setPoses: function(poses) {
3225             var child;
3226             if (poses[this._name]) {
3227                 this.set(poses[this._name]);
3228             }
3229             for (var i = 0, l = this.childNodes.length; i < l; i++) {
3230                 child = this.childNodes[i];
3231                 child.setPoses(poses);
3232             }
3233         },
3234         _applyPose: function(){
3235             var parent = this.parentNode;
3236             quat4.multiply(parent._globalrot, this._rotation, this._globalrot);
3237             quat4.multiplyVec3(parent._globalrot, this._position, this._globalpos);
3238             vec3.add(parent._globalpos, this._globalpos, this._globalpos);
3239         },
3240         _solveFK: function() {
3241             var child;
3242             this._applyPose();
3243             for (var i = 0, l = this.childNodes.length; i < l; i++) {
3244                 child = this.childNodes[i];
3245                 child._solveFK();
3246             }
3247         },
3248         _solve: function(quat) {
3249             quat4.normalize(quat, this._rotation);
3250             this._solveFK();
3251         }
3252     });
3253 
3254     /**
3255      * @scope enchant.gl.Skeleton.prototype
3256      */
3257     enchant.gl.Skeleton = enchant.Class.create({
3258         /**
3259          * Class that becomes bone structure route.
3260          * @constructs
3261          */
3262         initialize: function() {
3263             this.childNodes = [];
3264             this._origin = vec3.create();
3265             this._position = vec3.create();
3266             this._rotation = quat4.identity();
3267             this._globalpos = vec3.create();
3268             this._globalrot = quat4.identity();
3269             this._iks = [];
3270         },
3271         /**
3272          * Add child bone to skeleton.
3273          * @param {enchant.gl.Bone} child
3274          */
3275         addChild: function(bone) {
3276             this.childNodes.push(bone);
3277             bone.parentNode = this;
3278         },
3279         /**
3280          * Delete child bone from skeleton.
3281          * @param {enchant.gl.Bone} child
3282          */
3283         removeChild: function(bone) {
3284             var i;
3285             if ((i = this.childNodes.indexOf(bone)) !== -1) {
3286                 this.childNodes.splice(i, 1);
3287             }
3288             bone.parentNode = null;
3289         },
3290         /**
3291          * Set pose.
3292          * @param {*} poses
3293          */
3294         setPoses: function(poses) {
3295             var child;
3296             for (var i = 0, l = this.childNodes.length; i < l; i++) {
3297                 child = this.childNodes[i];
3298                 child.setPoses(poses);
3299             }
3300         },
3301         /**
3302          * Perform pose settlement according to FK.
3303          * Make pose from set pose information.
3304          */
3305         solveFKs: function() {
3306             var child;
3307             for (var i = 0, l = this.childNodes.length; i < l; i++) {
3308                 child = this.childNodes[i];
3309                 child._solveFK();
3310             }
3311         },
3312         /**
3313          * Add IK control information.
3314          * @param {enchant.gl.Bone} effector
3315          * @param {enchant.gl.Bone} target
3316          * @param {enchant.gl.Bone[]} bones
3317          * @param {Number} maxangle
3318          * @param {Number} iteration
3319          * @see enchant.gl.Skeleton#solveIKs
3320          */
3321         addIKControl: function(effector, target, bones, maxangle, iteration) {
3322             this._iks.push(arguments);
3323         },
3324         // by ccd
3325         /**
3326          * Perform pose settlement via IK.
3327          * Base on information added via {@link enchant.gl.Skeleton#addIKControl}
3328          */
3329         solveIKs: function() {
3330             var param;
3331             for (var i = 0, l = this._iks.length; i < l; i++) {
3332                 param = this._iks[i];
3333                 this._solveIK.apply(this, param);
3334             }
3335         },
3336         _solveIK: function(effector, target, bones, maxangle, iteration) {
3337             var len, origin;
3338             vec3.subtract(target._origin, target.parentNode._origin, _tmpinv);
3339             var threshold = vec3.length(_tmpinv) * 0.1;
3340             for (var i = 0; i < iteration; i++) {
3341                 vec3.subtract(target._globalpos, effector._globalpos, _tmpinv);
3342                 len = vec3.length(_tmpinv);
3343                 if (len < threshold) {
3344                     break;
3345                 }
3346                 for (var j = 0, ll = bones.length; j < ll; j++) {
3347                     origin = bones[j];
3348                     this._ccd(effector, target, origin, maxangle, threshold);
3349                 }
3350             }
3351         },
3352         _ccd: function(effector, target, origin, maxangle, threshold) {
3353             vec3.subtract(effector._globalpos, origin._globalpos, _tmpve);
3354             vec3.subtract(target._globalpos, origin._globalpos, _tmpvt);
3355             vec3.cross(_tmpvt, _tmpve, _tmpaxis);
3356             var elen = vec3.length(_tmpve);
3357             var tlen = vec3.length(_tmpvt);
3358             var alen = vec3.length(_tmpaxis);
3359 
3360             if (elen < threshold || tlen < threshold || alen < threshold) {
3361                 return;
3362             }
3363             var rad = Math.acos(vec3.dot(_tmpve, _tmpvt) / elen / tlen);
3364 
3365             if (rad > maxangle) {
3366                 rad = maxangle;
3367             }
3368             vec3.scale(_tmpaxis, Math.sin(rad / 2) / alen, _tmpquat);
3369             _tmpquat[3] = Math.cos(rad / 2);
3370             quat4.inverse(origin.parentNode._globalrot, _tmpinv);
3371             quat4.multiply(_tmpinv, _tmpquat, _tmpquat);
3372             quat4.multiply(_tmpquat, origin._globalrot, _tmpquat);
3373 
3374 
3375             if (origin.constraint) {
3376                 origin.constraint(_tmpquat);
3377             }
3378 
3379             origin._solve(_tmpquat);
3380         }
3381     });
3382 
3383     var DEFAULT_VERTEX_SHADER_SOURCE = '\n\
3384     attribute vec3 aVertexPosition;\n\
3385     attribute vec4 aVertexColor;\n\
3386     \n\
3387     attribute vec3 aNormal;\n\
3388     attribute vec2 aTextureCoord;\n\
3389     \n\
3390     uniform mat4 uModelMat;\n\
3391     uniform mat4 uRotMat;\n\
3392     uniform mat4 uCameraMat;\n\
3393     uniform mat4 uProjMat;\n\
3394     uniform mat3 uNormMat;\n\
3395     uniform float uUseCamera;\n\
3396     \n\
3397     varying vec2 vTextureCoord;\n\
3398     varying vec4 vColor;\n\
3399     varying vec3 vNormal;\n\
3400     \n\
3401     void main() {\n\
3402         vec4 p = uModelMat * vec4(aVertexPosition, 1.0);\n\
3403         gl_Position = uProjMat * (uCameraMat * uUseCamera) * p + uProjMat * p * (1.0 - uUseCamera);\n\
3404         vTextureCoord = aTextureCoord;\n\
3405         vColor = aVertexColor;\n\
3406         vNormal = uNormMat * aNormal;\n\
3407     }';
3408 
3409     var DEFAULT_FRAGMENT_SHADER_SOURCE = '\n\
3410     precision highp float;\n\
3411     \n\
3412     uniform sampler2D uSampler;\n\
3413     uniform float uUseDirectionalLight;\n\
3414     uniform vec3 uAmbientLightColor;\n\
3415     uniform vec3 uLightColor;\n\
3416     uniform vec3 uLookVec;\n\
3417     uniform vec4 uAmbient;\n\
3418     uniform vec4 uDiffuse;\n\
3419     uniform vec4 uSpecular;\n\
3420     uniform vec4 uEmission;\n\
3421     uniform vec4 uDetectColor;\n\
3422     uniform float uDetectTouch;\n\
3423     uniform float uUseTexture;\n\
3424     uniform float uShininess;\n\
3425     uniform vec3 uLightDirection;\n\
3426     \n\
3427     varying vec2 vTextureCoord;\n\
3428     varying vec4 vColor;\n\
3429     varying vec3 vNormal;\n\
3430     \n\
3431     \n\
3432     void main() {\n\
3433         float pi = 4.0 * atan(1.0);\n\
3434         vec4 texColor = texture2D(uSampler, vTextureCoord);\n\
3435         vec4 baseColor = vColor;\n\
3436         baseColor *= texColor * uUseTexture + vec4(1.0, 1.0, 1.0, 1.0) * (1.0 - uUseTexture);\n\
3437         float alpha = baseColor.a * uDetectColor.a * uDetectTouch + baseColor.a * (1.0 - uDetectTouch);\n\
3438         if (alpha < 0.2) {\n\
3439             discard;\n\
3440         }\n\
3441         else {\n\
3442             vec4 amb = uAmbient * vec4(uAmbientLightColor, 1.0);\n\
3443             vec3 N = normalize(vNormal);\n\
3444             vec3 L = normalize(uLightDirection);\n\
3445             vec3 E = normalize(uLookVec);\n\
3446             vec3 R = reflect(-L, N);\n\
3447             float lamber = max(dot(N, L) , 0.0);\n\
3448             vec4 dif = uDiffuse * lamber;\n\
3449             float s = max(dot(R, -E), 0.0);\n\
3450             vec4 specularColor = (uShininess + 2.0) / (2.0 * pi) * uSpecular * pow(s, uShininess) * sign(lamber);\n\
3451             gl_FragColor = (vec4(((amb + vec4(uLightColor, 1.0) * (dif + specularColor)) * baseColor).rgb, baseColor.a) \
3452                 * uUseDirectionalLight + baseColor * (1.0 - uUseDirectionalLight)) \
3453                 * (1.0 - uDetectTouch) + uDetectColor * uDetectTouch;\n\
3454         }\n\
3455     }';
3456 
3457 }());
3458