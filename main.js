enchant();

var game;

window.onload = function(){

	game = new Game(480, 320);	// Instantiate enchantJS game, assign to game variable
	game.fps = 30;
	game.enemy_speed = 2;
	game.preload(	"media/ship_1.png",
					"media/ship_2.png",
					"media/ship_3.png",
					"media/game_bg.png",
					"media/canon_ball.png",
					"media/island.png",
					"media/island_canon.png",
					"media/explosion.png",
					"media/water_droplet.png",
					"media/music_btn.png",
					"sounds/tankFire.wav"	
	);
			
	game.onload = function(){
		
		var background = new Sprite(480,320);
		background.image = game.assets["media/game_bg.png"];
		game.rootScene.addChild(background);
	
		var island = new Island(280,32);
		game.rootScene.addChild(island);
				
		var defender = new Defender(320,76);
		game.rootScene.addChild(defender);
		
		var life = new enchant.Label();
		life.width = 320;
		life.height = 64;
		life.x = 40;
		life.y = 20;
		life.font = "20px 'Arial'";
		life.color = "red";
		game.rootScene.addChild(life);
		
		var score = new enchant.Label();
		score.width = 320;
		score.height = 64;
		score.x = 280;
		score.y = 20;
		score.font = "20px 'Arial'";
		score.color = "red";
		game.rootScene.addChild(score);
		
		var strength = new enchant.Label();
		strength.width = 320;
		strength.height = 64;
		strength.x = 160;
		strength.y = 60;
		strength.font = "20px 'Arial'";
		strength.color = "red";
		game.rootScene.addChild(strength);
		
		var canShoot =true;
		var button1 = new Button(100,100);
		
		//var bgMusic = new DOMSound();
		//bgMusic.load('tankFire.wav');
		
		// Init game
		game.rootScene.addEventListener('enterframe', function() {
			 //game.assets['jump.wav'].clone().play();
			
			life.text = "Health : "+island.health;
			score.text = "Score : "+island.score;
			strength.text = "Strength : " + defender.strength;
			
			/*if(this.age % 27 == 0){
				if(defender.strength<12){
					defender.strength++;
					if(defender.strength==11){
						defender.strength=1;
					}
				}
			}*/
			
			if(this.age % 100 == 0){
				var i = Math.random();
				if(i<0.33){
					var enemy = new Enemy1(-128, 215);
				}else if(i<0.66){
					var enemy = new Enemy2(-128, 210);
				}else{
					var enemy = new Enemy3(-128, 225);
				}
			}
			
			var set0 = Bullet.intersect(Island);
			for (var i = 0, l = set0.length; i < l; i++) {
				var explosion = new Explosion(set0[i][0].x,set0[i][0].y-50);
				explosion.frame = [0,0,0,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,null];
				set0[i][0].remove();
				set0[i][1].kill(set0[i][0].damage);
			}
			
			var set1 = Enemy.intersect(Island);
			for (var i = 0, l = set1.length; i < l; i++) {
				var explosion = new Explosion(set1[i][0].x,set1[i][0].y-50);
				explosion.frame = [0,0,0,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,null];
				set1[i][0].remove();
				set1[i][1].kill(set1[i][0].damage);
			}
			
			var set2 = Canon.intersect(Enemy);
			for (var i = 0, l = set2.length; i < l; i++) {
				var explosion = new Explosion(set2[i][0].x-100,set2[i][0].y-50);
				explosion.frame = [0,0,0,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,null];
				set2[i][0].remove();
				set2[i][1].kill();
				island.win(set2[i][1].score);
			}
			
			if(island.health<1){
					island.remove();
					defender.remove();
					canShoot =false;
			}
			if(this.age%9==0){
				if(defender.strength>0){
					if(game.input.right){
						defender.strength--;
					}
				}
				if(defender.strength<10){
					if(game.input.left){
						defender.strength++;
					}
				}
			}
			
			
			
			
			
		});
		
		game.rootScene.addEventListener('touchstart', function() {
			game.assets['tankFire.wav'].clone().play();
			if(canShoot){
				var canon = new Canon(335,94);
				canon.getVy(defender.strength);
				defender.frame = [0,0,0,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,null];
			}
		});
		
	};

	game.start();
	
	orientationHandler();
};