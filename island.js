
var Island = enchant.Class.create(enchant.Sprite, {
	
	health:1,
	score:0,
	
	initialize: function(x, y) {
		
		enchant.Sprite.call(this);
		this.width=320;
		this.height=280;
		this.x = x;
		this.y = y;	
		this.image = game.assets["media/island.png"];
		this.scaleX=-1;
		this.frame = 0;
		
		//game.rootScene.addChild(this);
	},
	
	kill: function(){
		this.health = this.health -1;
		//console.log(this.health);
		if(this.health<3){
			this.frame = 4;
		}else if(this.health<5){
			this.frame = 3;
		}else if(this.health<7){
			this.frame = 2;
		}else if(this.health<9){
			this.frame = 1;
		}
	},
	
	win: function(){
		this.score =this.score + 1;
		//console.log(this.score);
	},

});