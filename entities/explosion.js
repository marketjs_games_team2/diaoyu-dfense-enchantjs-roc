var Explosion = enchant.Class.create(enchant.Sprite, {
	
	initialize: function(x, y) {
		enchant.Sprite.call(this);
		this.width=160;
		this.height=120;
		this.x = x;
		this.y = y;			
		this.image = game.assets["media/explosion.png"];
		this.scaleX=-1;
		this.frame = 0;
		
		this.addEventListener('enterframe', function() {
			this.update();
		});
		
		game.rootScene.addChild(this);
	},
	
	update:function(){
		if(this.age % 18 == 0){
			this.remove();
		}
	}
});