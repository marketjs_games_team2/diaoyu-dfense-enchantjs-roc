var Bullet = enchant.Class.create(enchant.Sprite, {
	vx:5,
	vy:-5,
	gravity:0.1,
	damage: 10,
	initialize: function(x, y) {
		enchant.Sprite.call(this);
		this.width=32;
		this.height=24;
		this.x = x;
		this.y = y;
		this.image = game.assets["media/water_droplet.png"];
		this.scaleX=-1;
		this.frame = 0;
		
		this.addEventListener('enterframe', function() {
			this.update();
		});	
		game.rootScene.addChild(this);
	},
	
	update:function(){
		this.vy = this.vy+this.gravity
		this.x = this.x+this.vx;
		this.y = this.y+this.vy;
	}
});