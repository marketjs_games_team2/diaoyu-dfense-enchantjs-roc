var Canon = enchant.Class.create(enchant.Sprite, {
	vx:6,
	vy:-10,
	gravity:0.5,
	initialize: function(x, y) {
		enchant.Sprite.call(this);
		this.width=12;
		this.height=12;
		this.x = x;
		this.y = y;
		this.image = game.assets["media/canon_ball.png"];
		this.scaleX=-1;
		this.frame = 0;
		
		this.addEventListener('enterframe', function() {
			this.update();
		});
			
		game.rootScene.addChild(this);
	},
	
	getVy: function(x){
		this.vy = -x;
	},
	
	update:function(){
		this.vy = this.vy+this.gravity
		this.x = this.x-this.vx;
		this.y = this.y+this.vy;
	}
});