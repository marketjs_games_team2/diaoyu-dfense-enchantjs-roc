var Island = enchant.Class.create(enchant.Sprite, {
	
	health:100,
	score:0,
	
	initialize: function(x, y) {
		
		enchant.Sprite.call(this);
		this.width=320;
		this.height=280;
		this.x = x;
		this.y = y;	
		this.image = game.assets["media/island.png"];
		this.scaleX=-1;
		this.frame = 0;
	},
	
	kill: function(x){
		this.health = this.health-x;
		//console.log(this.health);
		if(this.health<3){
			this.frame = 4;
		}else if(this.health<6){
			this.frame = 3;
		}else if(this.health<11){
			this.frame = 2;
		}else if(this.health<16){
			this.frame = 1;
		}
	},
	
	win: function(x){
		this.score =this.score + x;
	},

});