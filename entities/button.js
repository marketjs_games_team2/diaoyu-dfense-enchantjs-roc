var Button = enchant.Class.create(enchant.Sprite, {
	initialize: function(x, y) {
		
		enchant.Sprite.call(this);
		this.width=30;
		this.height=28;
		this.x = x;
		this.y = y;	
		this.image = game.assets["media/music_btn.png"];
		this.frame = 0;
		game.rootScene.addChild(this);
		
		this.addEventListener('touchstart', function() {
			this.frame = 1;
			
			
		});
	},
});