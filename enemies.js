// ENEMY BASE CLASS
var Enemy = enchant.Class.create(enchant.Sprite, {
	
	health:1,
	
	initialize: function(x, y) {
		// Inherit from Sprite class
		enchant.Sprite.call(this);
		this.scaleX=-1;
		this.frame = 0;
		
		// Event Listeners
		this.addEventListener('enterframe', function() {
			this.update();
		});

		// Add to root scene
		game.rootScene.addChild(this);
	},

	//kill function
	kill: function(){
		this.health = this.health -1;
		if(this.health<1){
			game.rootScene.removeChild(this);
		}
	},
	
	// Updates at specified game fps
	update:function(){
		this.x += game.enemy_speed;
	}
});

var Enemy1 = enchant.Class.create(Enemy,{
	health: 1,
	initialize: function(x, y) {
		// Inherit from Enemy class
		Enemy.call(this);
		
		// Assign width,height according to image dimension
		this.width=128;
		this.height=74;
		
		this.x = x;
		this.y = y;			
		this.image = game.assets["media/ship_1.png"];
	}
});

var Enemy2 = enchant.Class.create(Enemy,{
	health: 2,	
	initialize: function(x, y) {
		// Inherit from Enemy class
		Enemy.call(this);

		// Assign width,height according to image dimension
		this.width=132;
		this.height=96;
		
		this.x = x;
		this.y = y;			
		this.image = game.assets["media/ship_2.png"];
	}
});

var Enemy3 = enchant.Class.create(Enemy,{
	health: 3,
	initialize: function(x, y) {
		// Inherit from Enemy class
		Enemy.call(this);

		// Assign width,height according to image dimension
		this.width=118;
		this.height=72;
		this.x = x;
		this.y = y;			
		this.image = game.assets["media/ship_3.png"];
	}
});