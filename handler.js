function sizeHandler(){
	// Currently does nothing
}

function orientationHandler(){
	var orientation = window.orientation;
	switch(orientation){
		case 0:
			document.getElementById('orientate').style.visibility="visible";
			document.getElementById('enchant-stage').style.visibility="hidden";
			break;
		case 90:
			document.getElementById('orientate').style.visibility="hidden";
			document.getElementById('enchant-stage').style.visibility="visible";
			break;
		case -90:
			document.getElementById('orientate').style.visibility="hidden";
			document.getElementById('enchant-stage').style.visibility="visible";
			break;
		case 180:
			document.getElementById('orientate').style.visibility="visible";
			document.getElementById('enchant-stage').style.visibility="hidden";
			break;
	}	
}

window.addEventListener('resize', function (evt) {
	sizeHandler();
}, false);

window.addEventListener('orientationchange', function (evt) {
	orientationHandler();
	sizeHandler();
}, false);

document.ontouchmove = function(e){ 
    window.scrollTo(0, 1);
}