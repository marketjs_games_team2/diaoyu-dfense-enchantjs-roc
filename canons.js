
var Canon = enchant.Class.create(enchant.Sprite, {
	
	initialize: function(x, y) {
		enchant.Sprite.call(this);
		this.width=12;
		this.height=12;
		this.x = x;
		this.y = y;			
		this.image = game.assets["media/canon_ball.png"];
		this.scaleX=-1;
		this.frame = 0;
		
		this.addEventListener('enterframe', function() {
			this.update();
		});
			
		game.rootScene.addChild(this);
	},
	
	update:function(){
		this.x = this.x-3;
		this.y = 0.0056*this.x*this.x-2.67*this.x+349;
	}
});